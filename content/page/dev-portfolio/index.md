---
title: "Dev Portfolio"
date: 2021-03-09T16:35:10-05:00
draft: false
tags: [self, programming]
type: "dev-portfolio"
tagtargets: [programming, portfolio]
---

I develop software as part of my "thing." You can also check out my skills [over here](/page/skills/).

Due to a planet-wide shortage of me updating my blog, this list is incomplete.
