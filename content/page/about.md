---
title: "About"
date: 2020-05-19T21:35:10-05:00
draft: false
tags: [self]
featured_image: "/images/DSC00286-hero.jpg"
hidehero: true
---

![me, probably about 5 years ago](/images/steampunk-2-square.jpg)

I'm a strong proponent of continued learning, creativity, automation, personal data control and security, a detractor of vendor lock-in and abusive corporations. I'm opinionated about software. I think that we need more love in the world these days. If I had a personal social motto, it'd be "don't be a dick."

I'm a self-taught programmer and I can write html, javascript, css, C#, Python, a bit of PHP, whatever you call Hugo's templating system, and can pick up new languages fairly easily. I use scripting and automation to solve pretty much every problem I come across in my daily work. But more importantly, it's taught me to think even more analytically than before, which in turn helps every other facet of my life.

I've worked in the video game industry as a 3d artist and scripter and in software design as a UX/UI designer, both of which influence my way of thinking. I was a philosophy minor[^1] and I can be pretty pedantic sometimes, but I very much cherish clear and critical thinking.

I'm basically synonymous with [Arcandio](https://arcandio.com) and [Voidspiral](https://voidspiral.com). If you see those on the internet, they're probably me.

I collect hobbies. Besides tabletop rpgs, wargaming, and video games, I like to paint minis, bike, hike, camp, ski, read, cook, eat, and sketch, and I'm into 3d printing[^2], woodworking, worldbuilding, astronomy, archery, cosplay, shooting, outdoor skills, photography, and music. I'm very slowly learning sustainable hunting, HAM radio, guitar, music production, poker, ontology, and small engine maintenance.

I have a lovely family, an unknown number freshwater fish, 4 bizarre cats, and one sockpuppet of a snake.

I consider table-top role-playing games to be the highest form of entertainment, because they blend artistic expression, the innate human need for storytelling, the empathy to imagine yourself as someone else, and exercise for your imagination.

I'm bad at social media.

[^1]: I know.

[^2]: FDM *and* resin. Strongly prefer resin.