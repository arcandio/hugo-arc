taskkill /IM FoxitReader.exe

pandoc^
 -o html5^
 -f markdown_strict^
+markdown_in_html_blocks^
+auto_identifiers^
+implicit_header_references^
+strikeout^
+footnotes^
+inline_notes^
+pipe_tables^
+smart^
 --metadata pagetitle="Commissions by Joe Bush / Arcandio"^
 -H "style.css"^
 -s^
 -o "commissions.html"^
 "index.md"

prince^
 "commissions.html"^
 --pdf-title="Commissions by Joe Bush Arcandio"^
 --pdf-author="Joe Bush"^
 -o "Commissions-by-Joe-Bush-Arcandio.pdf"

xcopy "Commissions-by-Joe-Bush-Arcandio.pdf"^
 "C:\Users\arcan\Dropbox\Voidspiral Work\Commissions by Joe Bush Arcandio.pdf"^
 /Y