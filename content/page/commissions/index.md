---
title: "Commissions"
date: 2020-12-07T11:35:10-05:00
draft: false
tags: [art]
featured_image: "/images/character-lineup-2021-02-11.png"
aliases: ["/#commissions", "/commissions"]
---

# Commissions by Joe Bush / Arcandio

[Site](http://arcandio.com) | [Sketchblog](https://www.instagram.com/arcandio/) | [Portfolio](https://joebush3d.com) | [Print Store](https://www.etsy.com/shop/VoidspiralArt)

![Commission banner copy](character-lineup-2021-02-11.png)

Hi there! I'm [Joe](http://arcandio.com), and this is my commission information packet. You might pick this up before a commission or after. It shows you the options I provide if you're still looking, but it also tells you what I need from you once we get started. It's kind of a big document, but it covers pretty much everything, so bear with me. The PDF version is fully bookmarked, so you can pop open your bookmarks tab in your PDF reader and browse around as needed. **Don't miss this important bit though: [What I need from you](#what-i-need-from-you)**. If you're reading this in PDF, you can find the up-to-date version at [https://arcandio.com/commissions](https://arcandio.com/commissions).

You can also download a PDF of this page: [Commission Info Packet](Commissions-by-Joe-Bush-Arcandio.pdf)

<!-- I usually stream my commissions on [twitch](https://www.twitch.tv/arcandioart). Most of the time the streams are from about 8:00 am CST to 9 or 10 am. If you don't want me to stream your commission, let me know and I won't! -->

## Commission Status
<style>
  .status {
    color: black;
    background: darkred;
    text-align: center;
    padding: 1em;
    font: Exo;
    font-size: 2em;
    font-weight: bolder;
    box-shadow: 0rem .5rem .5rem #0005;
  }
  .status:before{
    content: "Closed";
    text-transform: uppercase;
    font: Exo;
    font-size: 2em;
    font-weight: bolder;
    text-transform: uppercase;
  }
  .open {
    color: white;
    background: green;
  }
  .status.open:before{
    content: "Open";
  }
</style>

<!-- <div class="status open"></div> -->

<div class="status open"></div>



> ## IMPORTANT:
> I can't guarantee the delivery date of any commissions ordered in December before Christmas.
>
> The turnaround for any given piece is about 2-3 weeks because of communication time, and if there's anything already in my queue, new pieces will naturally be done after those.

My commissions are sometimes closed. If you *haven't* already purchased one, you can check whether I'm available by going checking on the web at <https://arcandio.com/commissions>.

I've often got a number of commissions in the pipe at a time, which means there may be a waiting period, even if my commission status is open. You can get an idea of what's going on by checking out my [task board](https://gitlab.com/voidspiral/FreelanceProjects/-/boards/1049690)

## Stuff I Will Do

This commission packet/page is mostly aimed at 2d digital character illustrations delivered as JPG/PNG files. I do also offer 3d printing, mini design, and mini painting commissions now, which largely function the same, except that they don't have Etsy standard versions; they're handled as hourly commissions. In short, here's what I can do:

- 2d artwork
  - character portraits
  - landscape paintings
  - maps
- 3d artwork
  - mini design files
  - mods of digital mini files
  - 3d resin printing
  - bust design files
  - mini/bust painting

## Stuff I won't do

* Extreme Gore/vore. Mild violence is ok though.
* Hardcore or illegal NSFW. Liability reasons. Mild NSFW is ok though.
* **I reserve the right to refund you and cancel a commission I'm not comfortable with.**

# Commission Types

I offer 2 types of commission:

| Type                                          | Notes                                                        | Prices |
| --------------------------------------------- | ------------------------------------------------------------ | ------ |
| [Standard Commissions](#standard-commissions) | standardized, predefined features, handled through Etsy, agreed upon in advance, fixed price | $      |
| [Hourly Commissions](#hourly-commissions)     | customizable features, speculative/exploratory work, price based on hours worked, initial deposit required to start work | $$     |

Besides the information about the *type* of commission, I'll also need information about the character. This is one of the most important parts. If you want to skip ahead, [you can see those questions here.](#what-i-need-from-you)

> ### The Tradeoff
>
> Because I offer 2 different ways of pricing my commissions, it's possible to try to game the system: if your commission is very simple, small in scope, and well defined with good reference art, you might be better off doing an *hourly* commission, because there's a possibility that I might get it done faster than the standard, flat-priced version. But that's a risk, it could take longer than the standard price too.
>
> Conversely, if you ask for a ton of extras, elaborations, ornate clothes, three hyper-important rings, and somehow holding five weapons on a standard Etsy portrait, we'll have to start talking about hourly commissions.

## Standard Commissions

Standard commissions are handled through [Etsy](https://www.etsy.com/shop/VoidspiralArt?section_id=24797881), and are faster, simpler, cheaper option, with less bells and whistles. I try to deactivate these items if my [commissions are closed](https://arcandio.com/#commissions), but I may have missed one. If my commission status is closed, either avoid ordering at all (and favorite the commission you want) or realize that it may be a while before I can get to your commission.

**If you do purchase a standard commission, make sure to message me about it!** There are two ways to do this:

1.  **Etsy Messages.** Self explanatory.
2.  **Email the Service Desk.** This is the same system as the one for custom commissions. ([more info](#service-desk-info))

The reason I want you to message me is two-fold: It notifies me of the sale immediately (which doesn't happen if you just purchase the commission and *don't* message me) and it opens the conversation for what your commission is about.

**Note:** Standard commissions include **1 character only.** If you need more characters in your commission, we can do a Custom Commission.

Even if you'd like a standard commission, you might want to take a look at [my art styles](#my-art-styles) first to get an idea of what I offer. Most of them have a link to standard version of the commission on Etsy.

## Hourly Commissions

I do accept custom commissions not handled through Etsy. These are better for complex or non-standard projects, like book-covers, posters, interior art for games, and so on. Because they're usually more complicated and involved, they are often more expensive than Standard Commissions.

If I'm available for commission, you can set up a custom commission by emailing me at: [incoming+voidspiral/FreelanceProjects@incoming.gitlab.com](mailto:incoming+voidspiral/FreelanceProjects@incoming.gitlab.com). This is the service desk for this GitLab project. It will allow me to track the status of your commission and to communicate with you through it. You don't need to do anything aside from reply to the emails in your normal email client. ([more info](#service-desk-info))

For custom commissions, I only take Paypal at this time: <https://www.paypal.me/voidspiral>

My current hourly rate is $30/h. I understand that hourly charges can be intimidating for clients commissioning work, so I've included some historical data in the next section, but suffice it to say that most of my commissions take between 4 and 8 hours for me to complete, which comes out to $120--$240. The average is 5.67 hours, or $170.

One important advantage of this hourly rate is that it vastly simplifies the setup process. I don't need you to pick things off a menu, like I've had in the past. You can tell me exactly what you need, and I'll do it. If it sounds time consuming/expensive, I'll let you know.

### Hourly Commission Process

Hourly commissions proceed in the following steps:

1. Initial contact
2. Discussion of project & setup
    1. Pick a Rendering Style [shown here](#my-art-styles).
    2. Pick a Face Style [show here](#face-styles).
    3. Collect & send me some reference art that describes what you're after.
3. $50 Deposit Paid by client
4. Work Begins
    1. Repeated progress update images
    2. Repeated direction check
    3. Hourly work tracking
5. Work is Complete
6. Final Payment is calculated from hourly rate & hours worked
7. Final payment made by client
8. Final high-res work delivered

Why $50 for the Deposit? It's any easy, round number, and it's small enough that none of my hourly commissions have ever been less expensive than that. If you're ordering a sketch though, I might change the deposit since it has a chance of being less expensive.

### Setup Info

When we first talk about the project, we'll need to agree on the scope, art style, and face style. If you need the piece expedited, I can do that but it will cost an extra $20.

Scope is basically a measure of how complicated the piece will be. The bigger the scope, the more time the piece will take me, and thus the higher the final price will be. I'll let you know if your specific piece sounds like it's going to take a lot of time, but here are some factors:

Things that contribute to big scope:

* Multiple characters & interactions
* Multiple important items
* Complex, ornate, or layered clothing
* Nonhuman features, like horns or wings
* Complex backgrounds or scenes
* Amount of the character visible (ie Full vs Portrait)
* Special effects like fire, lightning, etc.
* Overall level of detail
* Lack of reference
* Multiple changes, revisions, or changing your mind

Note that these are all compounded, if you want multiple characters and each one has ornate clothing and wings, that's going to take a lot longer than a single character with less ornate clothing.

### Non-Character Art

I can also do non-character art as an hourly commission, such as landscapes, environments, objects, weapons, maps, and so on. The fundamental steps of the commission will be the same, but the discussion will naturally be more specific to the piece.

### How much will it cost?

The following data only includes pieces that I've recorded the progress of, and thus the exact times and dates. This isn't everything I've ever done, just a snapshot of 2019-2020 work, which should represent my working speed pretty accurately.

| Rendering Style |  Scope   | Avg Length | Count | Avg Price |
| :-------------: | :------: | :--------: | :---: | :-------: |
|      Anime      | Portrait |  3:36      |   3   |   $108    |
|      Anime      |   Full   |  3:42      |   7   |   $110    |
|  Oil Painting   | Portrait |  3:54      |   22  |   $117    |
|  Oil Painting   |   Full   |  7:30      |   8   |   $225    |
| Pencil & Color  | Portrait |  3:54      |   9   |   $117    |
| Pencil & Color  |   Full   |  6:54      |   6   |   $207    |

> For me personally, right now I'm trying to focus on Oil Painting style Portraits, because they're high quality but inexpensive and they give me the greatest creative freedom and challenge me the most.

### Overrun Prevention

I'm aware that you might have a budget you want to stick to, but if you do, it's best to tell me so that I can plan for completing the piece in your budget.

Additionally, regardless of whether you have a budget, you can also tell me at any time to wrap the piece up, and I'll go ahead and finish it to whatever quality I can immediately. That might take up to an hour, but that way you can control how much time I spend on the piece.

### Payment

I take [Paypal](https://www.paypal.me/voidspiral) for custom commissions. **Make sure my commissions are open and that you message me *before* you send payment:** I might be too busy to get to it any time soon. You can additionally check [my commission backlog](https://gitlab.com/voidspiral/FreelanceProjects/-/boards/1049690) and/or ask for an expeditied piece if time is an issue.

### Other Hourly Services

I do also offer other services on hourly commission, such as:

* Game Design
* Writing
* Graphic design
* RPG book layout / typography
* Game branding
* Kickstarter operations
* Asset management
* Production & publishing
* Programming / scripting / batch processing
* Image processing

Generally I'll provide as detailed an estimate as I can from the information you provide me, but that budget isn't binding, it's just an estimate of how long I think it'll take. Such projects have things that come up that will impact the time and price of completion. While laying out a simple RPG, for example, we may find out that I need to process a bunch of the art assets, which will take more time. I'll always keep you in the loop on issues that impact the price.

I almost always set up a [GitLab](https://gitlab.com/voidspiral) project for these sort of things, so that I can manage the project on a task-by-task basis. You'll be invited to the project and you can collaborate with me via the issues.

![1555090298143](1555090298143.png)

Using a task tracker like this is also helpful for showing you exactly what's taking up time and revealing the complexity of the project. Publishing is complicated and there are a ton of things that need to be done that most people aren't aware of. [Email me](mailto:incoming+voidspiral/FreelanceProjects@incoming.gitlab.com) if you're interested in an estimate.

With some projects, I may send an invoice early on, so that I can make sure my clients are serious. If you really want my confidence and enthusiasm, you can pay for part of the project up front, which we'll take off the total project hourly cost. Generally, I handle these projects using Paypal invoices.

# Getting Started

## My Workflow

I usually send sketches and roughs pretty frequently to make sure I'm getting your approval each step of the way. The earlier we catch a change, the more likely I'll be able to make it. For example, don't wait to ask about re-positioning an arm until after I've done the final lineart, etc.

1.  **Info.** You tell me what you're after. We figure out any details or important factors together. I may ask a bunch of questions before I get started. It'll help me a lot if you send along some reference art, either photos or artwork, that can help you describe what you're after.
2.  **Sketch.** I'll abscond and sketch out a rough. This *will* look crappy, but I'll show it to you so I can make sure I'm headed in the right direction with the pose and composition.
3.  **Roughs.** This is a series of steps, ranging from sketch refinements to color choices. I'll probably send multiple WIP (work-in-progress) images to you to get sign off on each step.
4.  **Completion.** I'll finish the work and send it to you. If the bill isn't settled, we'll do that first.

Digital files are delivered either by email or by Etsy message, depending on how we've been communicating.

## My Art Styles

There are two components to my art style: the rendering style, and the face style. They aren't necessarily tied together: I can paint you a realistic anime face, or a lineart realistic face if you want.

Here's a nifty chart for any art nerds that want to know more about my actual process. This isn't really intentional, it's more of the pattern that I tend to follow, so I may deviate if the piece calls for it.

![artwork decision tree](artwork-decision-tree.svg)

### Rendering Styles

Pick one, or select one of the images I've done. Prices are *per character*. A single image may consist of several characters, each of which adds to the cost of the commission.

<div style="page-break-after: always;"></div>

#### Painterly Style
* Fully Hand Painted, no lineart
* [standard version on Etsy](https://www.etsy.com/listing/632930964/painterly-character-portrait-commission)

<div class="renders">
<img src="Emar shargat.jpg" />
<img src="colton 2.jpg" />
<img src="Rinza Egrion.jpg" />
<img src="Varach Oss.jpg" />
</div>

#### Pencil & Color

* Pencil drawing with some hatching and shading
* digitally colored
* [standard version on Etsy](https://www.etsy.com/listing/646766331/line-color-character-portrait-commission)

<div class="renders">
<img src="Tifi commission.jpg" />
<img src="Anastacia.jpg" />
<img src="brigette.jpg" />
<img src="carmilla.jpg" />
</div>

#### HFA Style

* Thick, painterly lineart
* Pick a rendering style
  * cel-shaded coloring
  * painterly rendering style
* [standard version on Etsy](https://www.etsy.com/listing/646766331/line-color-character-portrait-commission)

<div class="renders">
<img src="2019-03-26 11_49_45-elegant.jpg - IrfanView (Zoom_ 594 x 918).png" />
<img src="2019-03-20 09_10_46-Ray.jpg - IrfanView (Zoom_ 709 x 918).png" />
<img src="2019-03-20 08_45_43-lawyer.png - IrfanView (Zoom_ 584 x 918).png" />
<img src="2019-03-20 09_59_10-basilisk.png - IrfanView (Zoom_ 2135 x 1990).png" />
</div>

#### Anime/Manga Style

* Thin lineart
* Pick a rendering style
  * Cel-shaded coloring
  * painterly rendering style
* [standard version on Etsy](https://www.etsy.com/listing/632941796/animemanga-style-character-portrait)

<div class="renders">
<img src="hama.jpg" />
<img src="z2 copy.jpg" />
<img src="nanami.jpg" />
<img src="h1.jpg" />
</div>

#### Lineart Only

* Pick thick or thin lineart
* Black and white or duotone only
* [standard version on Etsy](https://www.etsy.com/listing/658113692/black-white-drawing-digital)

<div class="renders">
<img src="2019-03-13 10_10_35-.png" />
<img src="2019-03-18 09_13_00-.png" />
<img src="2019-03-20 09_17_48-pazuzu.jpg - IrfanView (Zoom_ 615 x 918).png" />
<img src="2019-03-20 09_17_59-lashell.jpg - IrfanView (Zoom_ 725 x 918).png" />
</div>

#### Pen/Pencil Sketch

* sketchy, scrubby, rough
* Quick turnaround
* Black and white or duotone only
* [standard version on Etsy](https://www.etsy.com/listing/692156797/monochrome-sketch-digital?ref=listing_published_alert)

<div class="renders">
<img src="primarial 2 copy.jpg" />
<img src="jinn 10 copy.jpg" />
<img src="gemini 5 copy.jpg" />
<img src="sanguiver 3 copy.jpg" />
</div>


### Face Styles

This is how I draw the face. These don't affect the price. The face style is different from the rendering style: you can get a chibi face painted in the oil style if you want, etc. (Though I'm not good at that particular combination.)

#### Stylized
* My  usual drawing style
* Stylized but not excessively so
* Somewhat comic-book-ish

<div class="faces">
<img src="2019-03-20 08_39_43-commission tera.jpg - IrfanView (Zoom_ 709 x 918).png" />
<img src="2019-03-20 09_10_36-colton 2.jpg - IrfanView (Zoom_ 709 x 918).png" />
<img src="2019-03-20 09_10_52-Keschey.jpg - IrfanView (Zoom_ 918 x 918).png" />
<img src="2019-03-20 09_10_46-Ray.jpg - IrfanView (Zoom_ 709 x 918).png" />
</div>

#### Anime/Manga
* Big Tsurime / Tareme eyes
* Small, understated mouth
* Simplified/minimized nose

<div class="faces">
<img src="2019-03-20 08_45_43-lawyer.png - IrfanView (Zoom_ 584 x 918).png" />
<img src="2019-03-20 08_44_29-devourer.png - IrfanView (Zoom_ 612 x 918).png" />
<img src="2019-03-20 08_44_00-card.jpg - IrfanView (Zoom_ 594 x 918).png" />
<img src="z2 copy.jpg" />
</div>

#### Chibi
* Super Deformed character
* totes adorbs
* great for discord / twitch emotes and profile icons

<div class="faces">
<img src="chibi dorgana copy.jpg" />
<img src="chibi felista copy.jpg" />
<img src="chibi ciaris copy.jpg" />
<img src="chibi haula copy.jpg" />
</div>

### Landscapes

Landscapes work a little differently from character portraits. If characters exist in a landscape painting, they're not the focus. Instead, the focus is on the scene or environment.

<div class="renders">
<img src="painterly scifi landscape.jpg" />
<img src="journey.jpg" />
<img src="The Wastes.jpg" />
<img src="spearfield.png" />
</div>


### Maps

Like landscapes, maps don't involve character portraits. For a map, I'll need a rough guide to the locations and positions, as well as regional biomes and overall themes or style. For a map, let me know how big you want to be able to print it and whether or not it should have a grid.

<div class="renders">
<img src="D&D map.jpg" />
<img src="Floating Sky Islands.jpg" />
<img src="HF map copy.jpg" />
<img src="Islands and Archipelagoes.jpg" />
<img src="The Great Beasts Are Sleeping.jpg" />
</div>

### Size & Resolution

By default, I start each commission as a print-resolution 18x24" canvas. That means it'll be 18x14 inches or 457x609 mm at 300 ppi, for a grand total of 5400x7200 pixels. That's plenty big for any internet or screen use, or printing on letter-sized paper.

If you need your piece in a different size, just let me know *in advance*. Unless you need something that's seven feet tall and magazine-quality print, it'll be pretty easy for me to just start at a different size. **I can't resize the artwork after the sketch or lineart phase.**

If you're planning on having the piece printed, I recommend <https://www.Mpix.com>. I'm slowly working on a process for doing the printing and framing myself, so feel free to ask about that if you're interested, but it'll be pretty pricy.

Now that you've made up your mind on what *kind* of illustration you want, you need to answer me some questions.

# What I need from you

Besides what specifications you have for the piece (such as the rendering and face style) I need to know about the subject matter of the drawing.

The more I know about the piece, the more accurate I can make it. Here are just some of the usual questions.

> Note: I can't work *everything* into an image, but the more I know about something the better I can pick and choose details to make the image better.

* What's this character's **story**? 
    * Who are they as a person?
    * What's special about them?
    * What do they do?
    * What's their personality?
    * Are they from a franchise, game, show, etc?
* What are they **doing** in the image? 
    * Even if they're just standing there, are they handling something or looking somewhere?
* What are they **feeling** in the image we're showing? 
    * Are they mad, upset, content, suspicious, etc?
* What kind of **build** do they have? Are they muscular? skinny? bulky?
* What kind of **clothes** are they wearing? 
    * What kind of armor?
    * What kind of clothes are they wearing *besides* armor?
    * Are they carrying any items, objects, packs, weapons, shields, etc?
* What sort of **face** do they have? Is it smooth or blocky? 
    * What are their eyes like?
    * What sort of hairstyle do they have?
* What kind of **pose**? 
    * Are they looking at the viewer, at something else in the scene, or explicitly *away* from something?
* Is there any **symbolism** in the piece? 
    * Any symbolic objects or actions?
    * Any actual emblems, symbols, marks, or words?

## Bonus Points

Here are some additional tips for making things easier on us both. Do any of these and we will be friends. 

* **Bullet points** are good. Long, rambling paragraphs take time to re-read while I'm working.
* **Clear, specific descriptions are best.**
    * Purple prose doesn't help much, especially if it's vague. There's no need to be artful in your description, what I need is specificity, accuracy, and consistency.
* **Reference images** are king. If you've got past commissions, send them along. If you don't, your artist will deeply appreciate it if you send them some found photos that you can use to describe things. Find a picture of a face that's close to how you want your character's to look. This especially helps with clothes and equipment.
    * Even if the reference image isn't perfect, you can always tell me what you *don't* like about it, which is almost as helpful as a perfect reference photo.
    * It's perfectly fine to send other fantasy art or pieces by other artists. It can be especially helpful for things that you won't find normal pictures of.
* Try to keep things **organized**. Don't jump from hair to garments back to hair to face to socks. Start with the pose, then face and personality, then clothing and details. This is because I sketch a pose and face first, then "equip" the drawing as I refine it.
    * If you're using bullet points, put details in sub-bullets.
* Consider that some elements are **more important** than others. Make note of the important things, and make note if something is just nice-to-have gravy kind of stuff.
* **Personality**. Tell me what your character is *like.* What kind of stuff are do they do? What are they known for? Even if you're commissioning a T-posed character sheet, this will help with *at least* the expression, if not the whole piece.
* **Verbs**. What could they be doing in the piece? Think of it like a snapshot. The better snapshop would be while they're in the middle of an action, rather than just standing around.
* **Complex stuff is hard to draw.**
    * Be aware that you can't see the lower levels of multi-layered clothing.
    * Keep in mind what you paid for. If you bought a single character commission, you'll get push-back if you describe someone else participating in the scene.
    * Really small stuff might not show up well, especially if you want an image that shows the whole character, or it's positioned somewhere it'll be hidden or behind the body.
    * Often artists will leave some things out to help clarify the design. This is *especially* the case of RPG characters, who tend to be described as carrying much more stuff than would make sense or look good. Again, try to focus on the stuff that's important.
* **Body type** is a big help, especially if the artist shows a lot of different body types in their work. Keep in mind though that some artists are more comfortable with the body types they do most frequently. Reference here helps a lot too.
* **Ask questions** if you're unclear on something. I'd much rather chat about something than have you stress out over something you aren't sure about.
* **Changes are always easier earlier in the process.** Changes aren't necessarily bad, but keep in mind that it can be frustrating to go back over a bunch of tiny things over and over again, and that may take time away from other things.
* If in doubt about something, **just ask.**

If you've made it this far, thank you sincerely. Following basically *any* of these steps makes my job *so* much easier.

# Other Information

## Delivery

* I can deliver original Clip Studio Paint files if you want. I don't work in Photoshop, so there won't be a `.psd` file.
* Delivery will otherwise be in PNG or JPG format by email or etsy message, whichever we're communicating by.
* If for some reason the file is too big, I'll put it on Dropbox and send you a link, but I'll take that down eventually, so back it up. I keep commissions backed up myself, but I encourage you to make your own.

## License: Joint Ownership

We both have copyright on the finished piece. I retain copyright, so I can print and sell copies and put the work in my portfolio, but I also grant you the same copyright so you can do the same. That means you *can* use the work in a commercial project, get it printed a million times, use it for interior artwork in an RPG, license others to use it, or whatever, so long as we've finished the commission and you've paid for it.

## The fine print

* Revisions are limited to 3, so that I don't spend forever on any one commission.
* *I can't draw something you don't explain,* so communication is key. If you don't reply or email, I'll refund you and we can both move on.
* I try to sign all of my pieces (inconspicuously) now, but if you need yours unsigned for legal reasons, let me know.

## Service Desk Info

I encourage people who want to do custom commissions with me to communicate via the [Service Desk](mailto:incoming+voidspiral/FreelanceProjects@incoming.gitlab.com).

### What is it?

I use [GitLab](https://gitlab.com), a web-based app designed for software development for project management.

![2019-04-12 12_31_23-Window](2019-04-12_12_31_23-Window.png)

### Why would you do that?

1. It's easy. You don't need a Gitlab account or signin or anything. Just [email the service desk](mailto:incoming+voidspiral/FreelanceProjects@incoming.gitlab.com), then reply to the email as I respond.
2. It helps me out. It creates a task in my task tracker so I can assign tags and update its status.
3. I can include images and downloads in it.

### What exactly does it do?

* When you [email the service desk](mailto:incoming+voidspiral/FreelanceProjects@incoming.gitlab.com) gitlab catches the email and creates a private task on [my issue tracker](https://gitlab.com/voidspiral/FreelanceProjects/-/boards/1049690).
* Your task is added as a confidential issue by default. I won't make it public unless you ask me to.
* When the task is created, I get notified by email.
* When I comment on the task, that comment is emailed to you directly.
* When you reply to the email chain, your emails are added as comments.

## About Me

You can find out tons of stuff about me & my work (including my blog, live art streams, artist statement, and social media) on my website: <http://arcandio.com>. You can find my games on my company website: <http://voidspiral.com>.



[^footnote]: I'm also in the process of converting my commission info packet into a page on this website, but it's difficult to figure out how to generate the document from the webpage.