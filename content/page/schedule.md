---
title: "Schedule"
date: 2020-07-10T16:15:00Z
draft: true
featured_image: ""
hidehero: true
tags: [self]
---

This is what my day looks like. I update this as frequently as possible, because I use it to figure out what the hell I'm supposed to be working on in the AM.

| Batch      | Time          | Mon            | Tues           | Wed            | Thur           | Fri            |
| ---------- | ------------- | -------------- | -------------- | -------------- | -------------- | -------------- |
| My Work    | 7:30 - 8:30   | Daily Setup    | Daily Setup    | Art Practice   | Daily Setup    | Daily Setup    |
| My Work    | 8:30 - 10:00  |Commis&shy;sions|Commis&shy;sions| Deep Dive      |Commis&shy;sions|Commis&shy;sions|
| My Work    | 10:00 - 12:00 | Small Business | Small Business | Deep Dive      | Small Business | Small Business |
| Lunch      | 12:00 - 12:30 | Lunch          | TACOS          | Lunch          | Lunch          | Lunch          |
| Their Work | 12:30 - 4:30  | BBB            | BBB            | Deep Dive      | BBB            | BBB            |

* **Art Practice**
  * Personal drawing time
  * Gesture drawing
  * Masters Studies
  * Antum Art
  * Fangtales
  * Work on my weaknesses
  * practice practice practice practice[^1]
* **Commissions**
  * [Twitch Stream](https://www.twitch.tv/arcandioart)
  * [Commission Sales on Etsy](https://www.etsy.com/shop/VoidspiralArt)
  * [Commission Info](pages/commissions)
  * Antum Art
  * Fangtales
  * Channel Point Requests
* **Deep Dives**
  * "Heavy Lifting" design work
  * Game design focus time
  * Side projects
  * Pure creativity time
  * Podcasts
  * Video Editing
* **Small Business**
  * Game design
    * Antum
    * OVRPG
    * Ultimate System
    * Iconoclast
  * Software Development
    * [Spiral Game System](https://publish.obsidian.md/arcandios-notebook/SGS/_Spiral+Game+System)
    * https://arcandio.com
    * https://voidspiral.com
    * [TagRef](https://gitlab.com/voidspiral/tagref)
  * Social Media Upkeep
  * Emails
* **BBB**
  * Unity mobile application development


[^1]: practice