---
title: "Maker Services"
date: 2021-10-04T11:38:10-05:00
draft: false
tags: [art]
featured_image: "/images/20211004_084445.jpg"
aliases: ["/making", "/making", "/makerspace", "/maker"]
---

I've amassed a decent sized makerspace of my own over the years, and I offer my services for small-scale maker projects, arts, and crafts.

## Maker Service Status
<style>
  .status {
    color: black;
    background: darkred;
    text-align: center;
    padding: 1em;
    font: Exo;
    font-size: 2em;
    font-weight: bolder;
    box-shadow: 0rem .5rem .5rem #0005;
  }
  .status:before{
    content: "Closed";
    text-transform: uppercase;
    font: Exo;
    font-size: 2em;
    font-weight: bolder;
    text-transform: uppercase;
  }
  .open {
    color: white;
    background: green;
  }
  .status.open:before{
    content: "Open";
  }
</style>

<!-- <div class="status closed"></div> -->
<div class="status open"></div>


![prints](/images/20211004_084445.jpg)

## Overview

> this section is for people who aren't familiar with 3d printing & laser cutting.

Generally speaking, both laser cutting and 3d require a design file to be created ahead of time. Like printing a recipe on paper, you have to have the word doc first. File design isn't included with the printing, see below.

Once we've figured out the design, we can determine whether the object would be best created in laser cut, resin, or filament. Laser cut objects usually start out flat, and are assembled like Ikea furniture[^1]. 3d prints come out in a single piece, like a D&D mini or a small piece of jewelry[^2].

The process of printing the object usually takes a few hours. Depending on the complexity of the object, there's often a period of trial and error to get the print or cut precisely right. I don't charge per hour for cutting/printing, the prices for these are based on the size of the object.

Most of my work is artistic in nature, so the precise tolerances and clearances aren't that important. If you need something to be a precise size or dimension, let me know in advance so I can make sure to tune for accuracy.


## 3d & 2d Design

- Printing doesn't include file design, for that see the [commissions page.](/page/commissions) Normally, 3d designs & laser cuts are handled hourly.

## DLP (Resin) 3d Printing

![elegoo mars 2 pro](/images/20211004_100307.jpg)

- Machine: Elegoo Mars 2 Pro
- Max Part Size: 5"x3"x6" / 130x80x160mm
- Material: Water Washable UV-curing Photopolymer Resin
- All parts are cleaned and cured.
- Parts will come unfinished in medium gray.
- Extremely fine finish, good for D&D minis, jewelry, etc.
- Design File Format: .STL, .OBJ, .FBX, .BLEND

![various 3d prints from elegoo mars 2 pro](/images/20211004_115604.jpg)

|Max Length|Price per Part|
|:-:|:-:|
|1.5"|$5|
|2.5"|$10|
|3.5"|$20|
|5"|$40|
|6"|$60|

## FDM (Filament) 3d Printing

![creality ender 3 pro](/images/20211004_100311.jpg)

- Machine: Creality Ender 3 Pro
- Max Part Size: 8.5"x8.5"x9.5" / 220x220x250mm
- Material: PLA (Polylactic Acid) plastic filament
- Larger parts are MUCH more prone to failure[^3], so I may refuse some large, complex parts.
- Parts have layer lines, which might not be suitable for fine work or minis.
- Design File Format: .STL, .OBJ, .FBX, .BLEND

![Various FDM 3d prints from Creality Ender 3 pro](/images/20211004_115501.jpg)

|Max Length|Price per Part|
|:-:|:-:|
|2"|$5|
|3"|$10|
|4"|$20|
|6"|$40|
|8"|$60|

## Laser Cutting

![OMTech K40 C02 Laser Cutter](/images/20211004_100247.jpg)

Laser cutting uses a laser to cut intricate patterns or parts out of flat stock, usually about 3mm / 1/16" thick. I can also laser etch designs on flat objects.

- Machine: K40 C02 Laser
- Material Area 12"x12" / 300mm x 300mm
- Cut Area: 12"x8" / 300mm x 200mm
- Materials
  - 3mm Baltic Birch Plywood
  - 3mm Acrylic Sheet
    - Color availability varies, I probably get whatever color you need, but we'll want to check first.
- Note: Very thin parts will likely be burned away by the laser. Make sure that the closest 2 lines in the file are 2-3mm apart if possible.
- Design File Format: .SVG, .AFDESIGN
  - Cut lines should be a #FF0000 Red path
  - Vector etching should be a #0000FF Blue path
  - Raster etching should be black & white

![various 1" laser cut tokens in acrylic and baltic birch](/images/20211004_115922.jpg)
![2" magic circle in purple acrylic](/images/20211004_115937.jpg)

|Max Length|Price per Part|
|:-:|:-:|
|1"|$2|
|2"|$4|
|3"|$6|
|5"|$10|
|7"|$15|
|10"|$20|
|12"|$25|



## Getting Started

To get started, [Email my task tracker here.](mailto:incoming+voidspiral/FreelanceProjects@incoming.gitlab.com) We'll then figure out how to get you what you need!

[^1]: But with less instructions!

[^2]: Unless you want me to slice it into multiple pieces. Or hit it with a hammer I guess.

[^3]: Failure spaghetti.