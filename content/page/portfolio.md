---
title: "Portfolio"
date: 2023-03-01T01:00:00Z
draft: false
featured_image: "/images/oubliette_pasteup.jpg"
hidehero: false
tags: [self]
aliases: ["/portfolio", "/portfolios"]
---

I do a lot of things, so I've separated my portfolios out into three categories.

# Artwork

[Artstation](https://arcandio.artstation.com/)

[![Artstation](/images/oubliette_pasteup.jpg)](https://arcandio.artstation.com/)

# Game Design & Publishing

[Voidspiral Games website](https://voidspiral.com/)

[![Voidspiral Entertainment](/images/products_2023-03-01.png)](https://voidspiral.com/)

# Writing

- [Ex Eradicus Samples](/post/ex-eradicus-writing-portfolio)
- [Oubliette Samples](/post/oubliette-writing-portfolio)
- [Heroines of the First Age Samples](/post/heroines-of-the-first-age-writing-portfolio)

# Programming & Code

[Dev Portfolio page](/page/dev-portfolio/)

[![Dev Portfolio](/images/2020-10-29-15_17_24-F__freelance_repos_SGS_SGS-Console_bin_Debug_netcoreapp3.1_SGSConsole.exe.png)](/page/dev-portfolio/)