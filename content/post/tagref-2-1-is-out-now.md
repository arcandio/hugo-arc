---
title: "TagRef 2.1 is out now!"
date: 2020-07-01T9:32:18-05:00
tags: [programming, software, free, art, blog]
draft: false
featured_image: "/images/2020-07-01-09_24_23-TagRef.png"
hidehero: true
---

TagRef is a small app, similar to Gesture Drawing and TagSpaces, that allows you to organize and display your own curated reference images for practice sessions. It's primarily aimed at gesture drawing and figure drawing type scenarios, but you can use it for whatever sorts of practice you want.

In essence, it allows you to tag files, then filter files by words in their filenames, like tags, titles, or directory names. It then allows you to display those images in a timed format of your choosing, switching images randomly when the time is up.


[Get it here!](https://gitlab.com/voidspiral/tagref/-/releases/v2.1.0)

![tagref settings page](/images/2020-07-01-09_24_23-TagRef.png)

![tagref tagging page](/images/2020-07-01-09_23_53-TagRef.png)

![tagref display page](/images/2020-07-01-09_24_38-TagRef.png)

[Get it here!](https://gitlab.com/voidspiral/tagref/-/releases/v2.1.0)

# Why it's in Python

Astute observers may realize that I've already made TagRef before, and that this is version `2.1.0`. That's because I hate Electron, Node.js, and the whole stack of equipment that comes with it. It takes a ridiculous amount of setup and configuration. Python "just works" for the most part. Even accounting for the unfamiliarity of dealing with the Qt gui framework in Python, I got a *lot* more done *way* faster in python than I did with javascript.