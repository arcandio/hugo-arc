---
title: "Tags Not Categories"
date: 2022-06-15T7:07:46-05:00
tags: [blog, self, philosophy]
draft: true
featured_image: "images/20210120_101827.jpg"
hidehero: false
---

- everything's more complicated than it seems
- intellectual finesse


- functional vs oop programming in react
- types of games & gamers
- actors & artists
- cancel culture
