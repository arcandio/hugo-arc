---
title: "Recent Art and Woodworking"
date: 2020-08-28T11:47:46-05:00
tags: [art, woodworking, blog, home-improvement]
draft: false
featured_image: "/images/Tatra-hero.jpg"
hidehero: false
---

Well, it's been a while, blog. Hello again, how are you, I am doing... fine[^1].

# Artwork

Wow, a lot has happened since my last post. Way to be lazy.

I continued that stint of Oubliette character art for the ongoing Oubliette game I'm running, mostly the "villains".

![spoder lady](/images/Tatra.jpg)

![lord magna of the burning throne in the lake of fire and death](/images/magna.jpg)

![ok, but really he's pretty busy](/images/magna-making-of.jpg)

I also worked on some cool commissions:

![shifter commission](/images/alisha-shifter.jpg)

![undead orc](/images/christinas-Deianeira.jpg)

![vittoria van pyre. 3 guesses.](/images/nates-vittoria.jpg)

![dis dragom like the make metal](/images/michaels-zaffre.jpg)

and we had some fun with channel point sketches on [twitch](https://twitch.com/arcandioart/)

![tortle luchador](/images/turtle-luchador.jpg)

# Woodwork

Wow, a lot has happened since my last post. Way to be lazy.

Deja vu?


Ok, up first, a very simple little step stool for my wife, since we have a plate rail in the kitchen.

![a little stepstool](/images/20200809_092257.jpg)

Basically just some pine, pocket screws, and finish. Surprisingly light weight and hella sturdy.

I also spilled all my wood finish on the table with this one, so that's cool. Good job me.

Next up is a simple table for our deck. 

![deck table](/images/20200828_162830.jpg)

![deck table](/images/20200828_162824.jpg)

Yes, I'm still trying to use up all that extra deck wood. I'm getting close. Table's fairly sturdy, and heavy AF. Should sit still in all but the most northerly of hurricanes.

Up next: Shop project. Get the wood off the wall and over my head!

![overhead wood storage](/images/20200825_202817.jpg)

Man, hanging these in the afternoon in a closed garage on a 95&deg; day was ... sucky.

With this, I've got all of the 8' boards and the big sheet goods overhead and out of the way. I still want to get a new shelving unit built to hold my scrap wood and offcuts. I also need to build another couple of these overheads for the other side to hold the smaller bits of wood that are between 3' and 5'. Once I get all that cleared out, I'll have room to move some crap around and put up french cleats on that back wall for tool and item storage. Much excite.

# Going a little bit crazy

I'm pretty good at being at home and not having a lot of contact with folks, but it's wearing on me. Work seems like it's been the exact same week three weeks in a row now, and I'm getting burnt out on my projects. I don't even really have a solution, even if I take a day off or so, I'm still stuck in the same mess. Ugh.

[^1]: not really, but I haven't started a #mentalhealth tag on here yet.