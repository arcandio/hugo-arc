---
title: "Ex Eradicus Artwork Progress"
date: 2023-10-24T08:30:18-05:00
tags: [eradicus, art, cons, blog]
draft: false
featured_image: "images/demon-skull.png"
hidehero: false
---

> If you're looking for my actual portfolio, you can find it here: [arcandio.artstation.com](https://arcandio.artstation.com/)

Well, in between bouts of Gloomhome work[^1], I've been able to snag some time to start working on the real art for Ex Eradicus. Here are some pieces in no particular order, and a little bit of context about them.

# Rebelle Artwork

I've always loved Wayne Douglas Barlowe's work. Barlowe's Inferno, Brushfire, and Barlowe's Guide to Fantasy have long been some of my most important inspirational pieces. But it's a very hard style to imitate, and I've only recently had the confidence and tools to make the attempt. I've started using Rebelle occasionally, trying to work into a more naturalistic looking, organic oil painting feel.

![demon](/images/eradicus-rebelle-char-1.jpg)

A major part of what makes Barlowe pieces work for me is the incredible amount of surface detail he puts into things. Even a flat surface will be rendered with writhing fractal detail.

![rebelle landscape](/images/eradicus-test-print.jpg)

This piece I created in Rebelle, thinking that its organic brushes would let me emulate barlowe's style more. It's not even close to Barlowe's work, but it's at least evocative of the feel I want Eradicus to have.

![clip studio landscape](/images/landscape-2.jpg)

This piece I created in Clip Studio, to see how close I could match the thicc-paint style of Rebelle in Clip. Again, not very close to Barlowe, and not very close to the Rebelle piece, but interesting in ints own way. The sense of scale is harmed by my placement of details and poor atmospheric perspective usage. The squishies should have a *lot* more surface detail, and so should the ground. The army should be smaller and better laid out in perspective. The hive thing is pretty nice though, and I like the sky-mountains in the background.

![designing a creature](/images/designing-a-creature.jpg)

It's easier to draw in Clip, and that makes it faster for doing value paintings as the basis for detailed final painting. So on this one, I drew the thing out in as much detail as I could before moving into value painting, color, and layer breakup. Then I exported it as a PDF and imported it into Rebelle for the final painting. The final effect is pretty nice, but does look a little more like Clip than Rebelle.

# Heavy Metal T-Shirt Design

The other style I'm trying to tap into on Ex Eradicus is the graphic style of heavy metal T-shirts. I've used this for all the major villain illustrations and I've got some of them up on [Redbubble](https://www.redbubble.com/people/arcandio/shop?artistUserName=arcandio&iaCode=u-clothing).

These were all done in Clip Studio.

![tharuos](/images/tharuos-shirt.png)

![nihidion](/images/nihidion-shirt.png)

![cyred](/images/cyred-shirt.png)

![azech](/images/azech-shirt.png)

![abollezz](/images/abollezz-shirt.png)

![lezureos](/images/lezureos-shirt.png)

![praxogog](/images/praxogog-shirt.png)

![zhileroth](/images/zhileroth-shirt.png)

![malacoda](/images/malacoda-shirt.png)

![orgunogg](/images/orgunogg-shirt.png)


# Extra Art

![enemy forces](/images/enemy-forces.jpg)

![demon skull](/images/demon-skull.png)

![angel portrait](/images/angel-portrait.png)

# Gamehole 2023

![Gamehole eradicus battle](/images/PXL_20231022_134133066.jpg)

Gamehole was a blast this year. We didn't do as much in sales as we have in previous years, but a major portion of that was that we had some delays on Gloomhome that made it so we couldn't sell copies of it at the convention this year, as well as the fact that Eradicus wasn't ready to be printed yet. Such is life.

Anyway, I got some business cards into the hands of a lot of publishers, so here's hoping I can get some illustration work from that.

We also had 3 really fantastic demos, one of The Gloomhome Murders, and two of Ex Eradicus. The Gloomhome demo was a blast for everyone, and the players did a really good job of tracking down the clues. All the players we ran Eradicus with picked it up within about 15 minutes and were able to run about half of their first games by themselves with very little input from Liz and I, so we know the game works, even with people who have never played it before. We got a ton of positive comments from people we pitched it to, as well. If only we'd had copies to sell.

We're looking at more conventions, probably smaller ones in the southern wisconsin/northern illinois area for next year. We went to Garycon this year, but it was too pricy for us, and the culture wasn't quite right for a lot of our products.


[^1]: Which I've yet to post about *at all* it seems...