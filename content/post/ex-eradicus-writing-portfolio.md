---
title: "Ex Eradicus Writing Portfolio"
date: 2024-04-29T12:37:18-05:00
tags: [self, writing, portfolio]
draft: false
featured_image: "images/ex-eradicus-logo.png"
hidehero: false
---

This page has a few little snippets of writing from Ex Eradicus. They're not necessarily in order, so take each section separately.

> If you're looking to hire my services for writing, please contact me and I'll gladly send you any of my games to read as portfolio material.

Ex Eradicus is a miniatures skirmish game, so much of the writing is in the rules, which don't present very well without context. I've included some introductory and lore material instead.

---

# Introduction
The Gray Wastes
The worlds are dead. We stand on their ashes, gray clay underfoot, gray expanse of stone above. We are those left behind by the cataclysm, the debris of annihilation. Dust.

## What Remains
Some say that the clay and ash are aggregate souls, each fleck a lifetime or more. Now, eons after the cataclysm, the survivors have remade the land, shaping the clay and building pale reflections of their once-great universes. There is space aplenty. There is much time. The first to arrive have had spans innumerable to fashion new realms from the wasteland. Now they reach ever higher, building towers and cities to rival the dead heavens. As these realms are forged, new alliances and enemies are made.

## Armageddon's Survivors
Some fell through the cracks. Lost in the maelstrom, we passed into the abyss. If only one survived the death of each world, then that leaves an untold number, some fraction of infinity, roaming the wastes, searching for meaning. Or simply revenge.

Now many ally themselves together in bands, armies, even nations. Warlords rise and fall, heroes are forged from the fires of war and chaos. Loyalty is traded for power. Weapons are forged from iron sifted from rivers of tar. Spells are drawn forth from pools of chaotic energy. Artifices the like of which no mortal world would have ever permitted are fabricated in the halls of the ancient ones, and granted to their highest lieutenants. Creatures are shaped from the clay and sent into battle against them.

These are our times. Such is our world. This is Eradicus.

## Tools of War
You will definitely need the following items, but if you already play role-playing or tabletop war games, you will likely have some of this already.

- Some miniature models, usually monstrous or fiendish in nature
- 2-3 sets of polyhedral dice, from 1d4 to 1d20 & d% for rolling attacks, powers, and defenses
- 1 extra set of polyhedral dice for marking the remaining health of models on the battlefield
- Paper, character cards, character sheets, or devices to view stat blocks
- Tokens or models for objectives and items
- A ruler or measuring tape
- Scatter terrain, which could be as simple as books and boxes, or fully painted setpieces

Other items that will be helpful or enhance your gaming experience include...

- One or more printed terrain mats or terrain tiles
- Specific wall segments, obstacle tokens, and hazard tokens
- Enemy Archetype cards

## Terms
**Note:** More terms are defined and given page references in the Index/Glossary.

- **Attack**: Any active ability that can be used an an action that inflicts harm on the defender.
- **Power**: Any active ability that can be used as an action.
- **Model**: (N) Any *single* miniature figure.
- **Model(-ing)**: (V) The process of creating game rules that match well with the hypothetical properties of an object or character. Good modeling closely matches those hypothetical properties.
- **Unit**: Any uniform group of 2 or more models.
- **Item**: Any object a model is carrying. Can refer to a battlefield object, a weapon, armor, or power.
- **Buff**: A temporary Status Effect that enhances a model's stats or attacks
- **Debuff**: A temporary Status Effect that decreases or mitigates a model's stats or attacks.
- **Limit**: A permanent modifier to a model or item's XP and rules.
- **Natural Attack**: An attack made with only the *model's* stats, without using any items or powers.
- **Hero**: A model on the player's side. In RPGs, these characters would be called Player Characters or PCs.
- **Enemy**: A model opposing the player's side. In RPGs, these characters would be called NPCs.
- **Archetype**: A behavior system for enemy models.

## The Cataclysm
Approximately 3.6 billion years ago, the end of all multiverses began in a cascade of destruction. For each multiverse destroyed in this conflagration of nothingness, each neighbor was infected with a fresh calamity. This horrific process ended an infinite number of universes and worlds, leaving only the barest hint of evidence of having existed in the first place. But for more than three billion years those ashes built up and up as successive generations of cosmoses, thus forming The Gray Wastes, where the remnants of that which was have come to rest.

It has been 45,000 years since The Cataclysm slowed to an end. For 45 millennia the remnants have struggled to rebuild something in the Gray Wastes.

## The Gray Wastes of Eradicus
The ashy, stark realms of Eradicus are collectively referred to as The Gray Wastes. The Gray Wastes are composed of the ashes of uncounted dead universes. They form a post-cataclysmic metaverse not unlike an inverted section of interstellar space, where realms and worlds are voids of space embedded inside an infinite, granular volume of gray clay. Worlds are connected by gateway tunnels, which may be physical or metaphysical in nature. Individual worlds are often quite different from each other, though most still show telltale signs of being forged from the gray clay of the wastes.

## The Clay
The vast majority of all matter in The Gray Wastes is simple dead ash compacted by billions of years into a kind of rigid clay. This clay is the fundamental building block of the many worlds of The Gray Wastes. In some places it is carefully tended and nourishes strange fae plants into a verdant jungle. In others, it is baked and used for vast charcoal fortresses. It is even used by some entities to create new creatures from almost nothing.

## Categories
Categories are descriptive tags that are used to organize models into themes. They are not mutually exclusive, so you may find things that would fit into both Rot and Demon categories, or Angel and Undead categories for example. The following list of categories is not exhaustive, though many models will fit into some combination of them. You should feel free to coin new categories and factions as needed at your table. Each category is listed with a number of existing factions within it.

### Factions
Factions are separate organizations. Two factions may share the same category, but that does not imply that they are automatically allies. Many factions are enemies with other factions in their category. They may also have drastically different essences, plans, goals, tactics, or properties. It should also be noted that there are innumerable local factions that represent different areas of The Gray Wastes.

You will likely want to organize your forces in the course of campaigns or when purchasing miniatures. You can build your own personal lore by developing your models into one or more cohesive factions. The factions presented exist to give you an idea of what might exist in Eradicus and to explain some of the common models you might find in the hobby.

### Abomination Category
- **Veil Infiltrators:** abominations who penetrate mortal societies in disguise to influence affairs.
- **Star Eaters:** Vast slavering monstrosities from the cosmic void.
- **Anathemics:** A sect of occultists seeking blessings and secrets from beyond mortal comprehending.
- **Cosmodiacal Soldiers:** A militant wing of abominations seeking to subjugate the inferior lower planes of existence.

### Demon Category
- **Lilith's Children:** A tight-knit and protective group of succubi, incubi, and other religious exiles.
- **Ordo Maleficarum:** Disciplined demon knights who honorably serve Mahlat the Judge.
- **The Retaliators:** Rebellious fiends who strike out at the holy in vengeance for their abuse in the previous worlds.
- **Sorcerers of Infinity:** A cult of magicians seeking the power of demons by supplication or guile.
- **The Horde of Destruction:** A vast army of demons following Tharuos, the god of destruction.

### Rot Category
- **The Corpulent Maw:** A banquet of cadaverous gourmands who strive to outdo each other in their selections of rotting flesh.
- **The Everhungering:** A cabal of cursed vampiric monsters and demons that must either consume or decay.
- **The Plague Archivists:** A sickening group of physicians who cultivate, refine, catalog, and deploy the most insidious diseases.
- **The Pale Swarm:** An infectious horde of limpid zombies from every species and world.

# Known Archvillains
## Abollezz
*Maestro of Alteration, The Essence Sculptor*

The Essence Sculptor is an abomination from the outer interstices between universes who can alter the spiritual and physical makeup of beings to suit his whim the way a surgeon replaces organs. Abollezz appears as a corpse-thin humanoid made of malignant ectoplasm, twisting flesh, and numerous spines, blades, and fingers. Though he rarely moves against other large forces, sometimes his desire to experiment encompasses entire factions, bringing him into conflict with them.

**Goals:** Like many cosmic outsiders, Abollezz is utterly detached from morality, and cares nothing for the effect his operations have on his victims. He seems perpetually ambivalent towards his targets and creations, but will continue to pursue entities he wishes to sculpt to any end. He considers his work akin to art and science, ever curious to see how far he can alter a spirit or body.

**Forces:** Abollezz mostly employs other, lesser abominations and outsider horrors to acquire his targets, but there are also many cults that wish to garner his attentions for their own purposes. From among these, he selects trusted lieutenants to lead his forces, often twisted, unspeakably altered priests and insidious, stalking atrocities.

**Tactics:** The forces of Abollezz usually avoid direct conflict when they can. They prefer to lob spells at range, distract with illusion, and assassinate when necessary. Nevertheless, he is quite capable of designing custom battle horrors for any scenario his forces encounter.

**Relationships:** Abollezz's name is not usually found in war records, and is instead more prevalent in the annals of political intrigue and diplomacy. His forces are small compared to many other archfiend warlords, but his work is well respected, especially among the Draghignazza dynansties. He could certainly heal the wounds the Cataclysm wrought on the likes of Elspeth Orcades and others, but he as no interest in such things. Some of his creations are now infamous in their own right, like Malacoda, who Abollezz sculpted from a mere human monk many millennia ago.

## Orgunogg
*The Cannibal, Hightusk*

While none would think of Orgunogg when asked about the most ardent philosophers in the Gray Wastes, he is nonetheless one of the most focused, dedicated beings associated with the greenskin factions. It just so happens that his philosophy can be summed up as "eat them."

**Goals:** It would be folly to claim that Orgunogg has any kind of plan. He reacts to what he sees and hears, but he has almost no concept of foresight. If he sees something he wants to eat, he eats it, even if it's one of his own generals. It is quite likely that the movements of Orgunogg's forces are more directed by his ranking minions than himself, as they strive to find things more appealing for him to eat than themselves.

**Forces:** Orgunogg's forces are composed of hungry goblins, orcs, orgres, and giants, mostly armed with the most basic of equipment, all of whom are ready to feast on anything they can catch, be it friend or foe. In this chaotic stew of personalities, only the strong survive and rise to glory. There are no ranks or officers in Orgunogg's forces, but there are some who attain the same kind of respect by eating enough detractors. Chefs, however, are always held in high regard.

**Tactics:** Orgunogg's forces always focus on either the most delicious foes, or the lowest hanging fruit. Very few have magical prowess, though a number have developed special attacks or use sinister equipment.

**Relationships:** Orgunogg is friendly with any force or being that will bring him delicacies for exactly as long as the meal lasts, and as such, his allegiance is cheaply bought and quickly lost. He is known to follow in the wake of the armies of Tharuos, feeding on the "leftovers." Most do not choose to sacrifice their own for his pleasure and see him as an boorish villain.

## Cyred Kartrok
*The Extinguisher, Flame of the Cataclysm*

Cyred Kartrok is a scorched shell of an angel, hollow of cinder flesh and wreathed still in the embers of his fall from heaven. He claims that he is responsible for the Cataclysm, and he says that the task is not yet finished.

**Goals:** The Extinguisher deeply desires the total destruction of all survivors of the Cataclysm. He focuses his attention most acutely on direct survivors, like Elspeth Orcades and Tharuos, but also on any settlement that can produce new offspring.

**Forces:** Kartrok's forces are composed of undead, angels, and undead angels. Many such beings are little more than automatons kept moving with the barest hint of necromancy, as it is his policy to reduce the population as efficiently as possible. Nevertheless, many of his warleaders are unique, sentient angels dating back to very ancient days. Occasionally, the Flame of the Cataclysm culls the eldest leaders in a "retirement ceremony."

**Tactics:** Cyred Kartrok's forces are not expansive. To make up for this, they are efficient, disciplined, judicious, evasive, and *highly* trained. He fields no minions, and each of his warriors would be equivalent to a specialist or elite soldier in another army. These forces usually play for maneuverability, range, and hit-and-run tactics, making them very difficult for melee-oriented armies to deal with.

**Relationships:** Kartrok is always warring with someone. Among his favored targets are Nefedius the Monument Builder (who clearly has different opinions about the Cataclysm) and the Lady of Avarice (who is quite comfortable here in the Gray Wastes). He has few allies, but has on rare occasional worked alongside Tharuos the Destroyer, Azech the Sorcerous Poison, and other omnicidal maniacs.
