---
title: "The Beginnings of SGS"
date: 2020-10-29T15:19:08-05:00
tags: [blog, game-design, programming, software, c-sharp, sgs]
draft: false
featured_image: "/images/2020-10-29-15_17_24-F__freelance_repos_SGS_SGS-Console_bin_Debug_netcoreapp3.1_SGSConsole.exe.png"
hidehero: true
---

It's getting cold out there. I've already about packed up the shop. The car lives in the garage too now, so there's only so much space for [#woodworking](/tags/woodworking/) now. I've gotten a new space heater, a nice new work jacket, and some heavy duty work pants, but I still think the winter's gonna mostly put the kibosh on working in the garage. I might even try to put up drywall on the ceiling this winter if I start to go stir crazy. So I'm going to try to shift my creative hobby energies over to 3d printing and minis again. That's a nice, warm, enjoyable winter hobby. But since I've had some problems enjoying D&D lately, I'm also trying to ramp up my own [Spiral Game System](/tags/sgs/).

# What's Spiral Game System?

SGS is sort of my fix-fic for RPGs. I'm putting everything I like about RPGs into it and taking everything I don't. It's most heavily inspired by Silhoutte Core / SilCore and Fate Core. I really like the flexibility of Aspects & fate points, but I also love how mathematically sound SilCore is. (I've even written about that [in the past.](/post/why-i-love-analytical-game-design/))

SGS is the game that ***I*** want to play. So, as usual, I'm making it myself.

# Core Features

- All stats are optional
- Characters, equipment, vehicles, monsters, etc. share the same set of viable stats
- simple roll mechanic
- generic system that supports customization for specific campaign settings
- No huge hit point pools
- Combat where heroes get weaker as they're hurt. No more "I'm perfectly healthy until I'm dead" from D&D
- Inherently balanced because all components are built using the same kind of Xp points
- Recursive entities: character has equipment, stats, and vehicle, equipment & vehicles have stats, vehicles have equipment, etc.
- Completely customizable stats, skills, equipment, health, defenses, etc.

# Hello World

![google docs](/images/2020-10-29-15_35_11-Window.png)
![google docs](/images/2020-10-29-15_37_59-Window.png)

Now, as usual, I started with a google doc, but because of that recusion feature, Google Docs ended up not being feasible for very long. The other problem there was that I couldn't iterate on the rules very quickly, because I had to rewrite every page of the spreadsheet every time I changed something. So we're not doing that.

I spent some time trying to figure out what the optimal system would be for this, which induced me to think about what pieces of software I'd want in order to support SGS. Here's what I decided.

- SGS Core (core library)
- SGS Console (testing & rapid prototyping)
- SGS Mobile
  - Android
  - iOS
- SGS Desktop
  - Windows
  - MacOS

After considering Javascript/Node.js, Python/PyQT, and a handful of other lest popular technologies, I ended up coming back to [C#/.Net/WPF/Xamarin](/tags/c-sharp). I've done a fair bit of C#/Mono for Unity projects over the years, and back in the 2012ish era I also got my hands dirty with a bit of WPF. WPF is extremely well documented, and I'm probably *most* comfortable in C# (at least that's what it looks like on my [skills page](/page/skills/)).

Now, so far, this has proved a good choice. I've learned a <mark>**huge**</mark> amount stuff about software development and C# by finally programming a project starting with the data structure / business logic first and working towards the UI from there. It hasn't been easy, but when have I ever done the easy thing?

# Development Progress

Right now, I've got a decently complete version of the core rules library, a passable interactive console application[^1], an auto-generated skeleton of an Xamarin Android app, and the basics of a windows WPF app. I've already changed the design of the game a few times and it's been relatively painless to do compared with debugging a spreadsheet that wants to be a database application.

![](/images/2020-10-29-15_17_24-F__freelance_repos_SGS_SGS-Console_bin_Debug_netcoreapp3.1_SGSConsole.exe.png)
![](/images/2020-10-29-15_17_41-F__freelance_repos_SGS_SGS-Console_bin_Debug_netcoreapp3.1_SGSConsole.exe.png)

I've also got some unit tests completed, but only for a really specific subsystem that I wanted to test before implementing. Once the codebase stabilizes[^2] I can do more unit and system testing, but for now things are too volatile to spend time setting up tests.

![](/images/2020-10-29-16_11_02-Window.png)

For now, I'm focusing on getting things at least partially half-assed in the Desktop version so that I can figure out what in the core library needs to change. I'm not familiar with databinding, and that's really calling the shots for the core library. After I get the desktop app mostly figured out, I'll probably have to switch over to the Xamarin version to test implementation over there too, for the same reasons. I think that there are some differences from desktop to Xamarin mobile WPF.

![](/images/2020-10-29-15_10_59-SGS-Entity-Browser---Kevlar-Reinforced-Suit.png)
![](/images/2020-10-29-15_11_26-SGS-Entity-Browser---Kevlar-Reinforced-Suit.png)

And at some point, I want to implement saving and loading via Google Drive & Dropbox. That way, I'll have a system where I can design a campaign & write up characters on my desktop in the office, then host the game and look at notes from my phone or a tablet. DANG that'd be slick.

Eventually, users should also be able to design their own rules-sets for SGS too, meaning you can change the stats, skills, calculations, etc. for products based on SGS, and the application suite will still work for that game.

Anyway, you can expect SGS to invade the next series of Voidspiral games, including [Antum](/tags/antum/), Wa 2.0, my own D&D clone, ATSD, and more.

---

# Birthday coming up

Normally I wouldn't write about this, but since it's on [**ELECTION DAY**](https://www.vote.org/) there's something I really want. Maybe you can help. Please vote. For the love of all that is good. Please.



[^1]: A ton of fun. I should write some text-based games like this. The loop-and-parse-input thing was a blast.
[^2]: I need to mock out stuff in the Desktop and Android projects so that I can make sure that the core library supports them