---
title: "Tablsaw Storage"
date: 2020-06-08T14:40:34-05:00
tags: [blog, woodworking, home-improvement]
draft: false
featured_image: "/images/20200608_135135.jpg"
hidehero: true
---

We finished[^1] the deck last Sunday, which is fantastic, and it looks really nice:

![our nice new deck and some tiki torches](/images/20200607_205504.jpg)

But unfortunately, that's not the end of the project. We're left with a ton of extra wood that I don't have a home for, as well as several big new tools I'm having trouble storing.

![a mess in the garage](/images/2020-06-08.jpg)

Among the largest new offenders is my brand-new table saw, which comes with its own fold up, roll-away cart, which is very cool, but the thing takes up space we don't really have at the moment, and I'm still trying to figure out what to do with the wood.

I've got a small list of other projects to build around the house, ranging from benches for the new deck and patio to storage shelves in the shed. So I figured I'd use some of the wood to create a way to store the table saw out of the way while regaining some of the floorspace it eats up.

![A rough and ready tablesaw storage unit](/images/20200608_135135.jpg)

Now, I'm no carpenter, but this works pretty well, and it's pretty sturdy. I only fractured one board by not pre-drilling my holes, but I can be forgiven for my laziness on a 92&deg;f day.

The best part is that I didn't need to go to the store for any of it, it's constructed purely from deck scraps and ancient plywood from approximately 347 BCE.

Now I just need to reorganize the entire garage so I can fit some 12' boards in there somewhere.

[^1]: for a given definition of done, which is to say that it still needs some sanding, rounding over the edges, and some stain, but it's done enough for now, damn it.