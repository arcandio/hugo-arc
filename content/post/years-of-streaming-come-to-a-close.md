---
title: "Years of Streaming Come to a Close"
date: 2021-05-14T09:35:19-05:00
tags: [blog, art]
draft: false
featured_image: "/images/2021-05-14-09_51_03-Fullscreen-Projector.png"
hidehero: true
---

![bye](/images/2021-05-14-09_51_03-Fullscreen-Projector.png)

I've been streaming art for about four years now, but I think I'm going to stop.

At the very least, I'm going to stop streaming on schedule. But it's also likely that I'm mostly going to stop streaming all together. I'd like to say that I'd occasionally stream some things, but I don't think that'll be the case, at least not with the way I'm feeling right now.

# Why Stream in the First Place?

So my goal is to make TTRPGs and get better at art. For a while, I operated under the assumption that I could just make games and that the free market would make them as popular as they would be, naturally. But that's simply not the case. You need to have a community in order to get the word out to a wider audience.

So I tried for a while to figure out what would work best as my social media / community building activity, and I settled on streaming. I thought that watching my art process and talking with me in realtime would be interesting enough to draw a few people in and start building a community.

# Real Talk

I guess it worked for a while? I made affiliate, met some folks, talked a lot early in the pandemic, but after people got back to work, my traffic dropped back to close to zero. For the past few months I've worked on my audio, getting my camera dialed in, and kept banging away in the art mines, and I've had very few people show up, and probably only a handful of new folks have come by in 2021.

There are at least 2 major contributing factors:

- I only have the energy to stream in the morning. If I do a full day of work and then try to stream in the afternoon, I'm out of energy to even *draw* let alone stream and chat at the same time.
- I stream *drawing* not some uberslick game. My perception is that people don't really care about that in the first place. The fact that I usually have a pretty steady stream of commissions to work on indicates that people like my work enough to pay for it, but the drawing process just isn't interesting to very many people.

# Why Stop Now?

No viewers. Simple as that. The only folks who show up to my streams are typically my few friends, and we don't usually talk about much.

I had a wave where we had 3-5 viewers consistently on stream daily for a few months, but I think that was just Covid talking.

Now my stream is a ghost town. I've got close to 100 followers, but only one really has the time to show up for stream on a semi-regular basis.

Now, if it weren't extra work to stream instead of just drawing, then I guess I'd worry about it less, but streaming ain't free. It takes time, energy, attention, and skill that I apparently don't have. I'm super jealous of people who have those capabilities.

> It's worth noting that I've always felt that you can do anything if you put your mind to it, but this is the first real time that I've been proven solidly wrong. Doesn't feel great.

# A Bigger Problem

This whole thing seems like a symptom of a larger issue for me, which is that nobody cares how hard you work, or what you're doing, they only want the perfect, finished end product (which they'll discard after a few cursory seconds of attention).

I work for myself, and it's really, really hard to get *any* kind of validation. Even when I work really hard on a commission, it's still pretty rare that people are actually pumped to see my progress. A lot of the time, I get fairly non-commital "sounds good" or "no changes so far" kinds of replies. And of course, nobody's impressed about game design until they play it, and that takes ages, especially when I've got two other jobs to work on aside from that. And even then, it still seems like people would rather play either A) D&D or B) not with me. As a game designer, that kind sucks preeeeety hard.

I don't honestly know how I can keep doing this (or anything) without *some* kind of validation or appreciation.

# All That Work

I put a ton of work and money into streaming. I built a very nice green-screen track that tucks away neatly behind my office door, and I spent hundreds of hours dialing in on the right wecams, lights, peripherals, and stream setup. Pretty much all of that is meaningless if no one comes to watch. I even had a friend who does my social media set up posts to remind folks that I'm live.

All just a big money pit.

My anxiety frequently revolves around what I could have done better. At this point, I think it's soured to depression because it sure seems like there *wasn't* really anything I could have done better. That was my best, and it wasn't good enough.

# What Now?

I don't know what'll survive the cull. I feel like quitting a lot of the stuff that was supposed to help me build community:

- Stream is empty and has zero effect on the community's attention
- Social media still seems to have no effect either
- Podcasts were only marginally successful
- Videos never really got any viewership
- Commissions feel like a treadmill, and in combination with streaming I don't have time to actually *practice* anything

Maybe I can try to keep doing a couple of these things. The podcast had some listeners, so maybe we can keep doing that. I guess I'll probably keep doing commissions in the future, but I'm going to close them now for a while in order to try to figure out what the fuck I'm doing.

# To those who were there

I really appreciated all the people who took time out of their day to hang out with me and chat, ask questions, or just chill. I really liked hanging out with you all. It made me feel like I wasn't doing all of this in complete isolation.

From the bottom of my heart:

thanks for watching everyone!

peace!

🙂