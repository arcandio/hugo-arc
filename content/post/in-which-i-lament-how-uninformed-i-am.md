---
title: "In Which I Lament How Uninformed I Am"
date: 2019-03-11T07:18:38-05:00
tags: [blog, self, tabletop-rpgs]
draft: false
featured_image: ""
hidehero: true
---

> I know what I Like  
> And I like what I know  
> -- Genesis

**I need to play more games.**

Look, I'm trying ok? We *all know* I've got [too many hobbies](/post/deep-vs-wide), I'm self-centered, lazy, and I'm stuck in my own silo.

## The Core Problem

A common adage is that to be a good writer you have to be a good reader. There are a lot of reasons to follow this advice.

* It provides a frame of reference.
* It shows you what others are doing.
* It lets you in on fads and memes.
* It helps you calibrate your work against the work of others.
* It helps you learn how to execute your ideas.
* It can show you new ideas you hadn't thought of.
* Competition & market research.

And here's the thing. Like just about everything I talk about here, it overlaps art, writing, and game design all at the same time. 

But I don't do that. Not much. Not enough, at the very least. I don't read many new games, and I don't hear about many new games very often.

* Part of this is how Twitter has functioned for me, by providing me an endless tide of content I can't possibly keep up with that shines so brightly that I can't bring myself to focus on it. I should write a post about that too. I'm trying to change this in part by participating more in [Tabletop.social](https://tabletop.social), where the talk is a more focused, there's slightly less looming greatness, and politics and other anxiety-inducing subjects come up less and can be <abbr title="content warning">CW</abbr>ed.
* Part of it is also that we've got a tight budget because my work doesn't bring in a lot, and so I don't purchase games very often. I'm just starting to keep an eye out for sales and bundles.
* Part of it is my hesitation to purchase games purely as reference material for my own work. This is changing, though.
* Part of it is of course that I don't read much because I have so many other hobbies to pursue.
* Probably the biggest part is my comfort in my own space, I.E. the Genesis quote above. This is a hard one for me. I love new books, movies, and music, but I'm also pretty comfortable listening, watching, or reading the same thing over and over again.
* A small part of it is that I don't have the luxury of playing or running games as often as you'd imagine a [#ttrpg](/tags/tabletop-rpgs) game designer to. I don't technically have to *play* the games I read, so long as I can absorb some of the points and elements of them.

It really bothers me, too. I do sort of miss the days when I was young and naive and designing games was fun. People do say that you should never do what you love as your job. Which is *exactly* what I'm doing right now. And maybe that's part of the problem: I may be putting too much stress on myself to be better than I can be at the moment. But what's the point of *not* wanting to improve?

## What I'm Trying To Do

* Read for work
  * I'm starting to purchase things I think might be useful for me to know about for the business. For example, I've not done any major pre-designed adventure module style content for Voidspiral games, and I've already posted about [Strahd](/post/my-experience-with-strahd-so-far), so I figured I'd pick up a modern module just to see how it's constructed and designed. I picked Out of the Abyss, because of course I did.
  * I also nabbed a huge stockpile of Pathfinder stuff from the Humble Bundle sale, but stars know when I'll find the time to read through that. At least it's PDF, so I could load it onto my phone.
* Play & run more
  * Now that I'm feeling better, I plan to kick my GMing back into gear, if only with my wife. We've got three games I'm GMing at the moment, and I can definitely spare some time to get some GM practice in, instead of us playing computer games or watching TV during our after-dinner R&R time.
  * It may be time for me to start looking for groups either in town, on Roll20, or via forums or something to get more of my fix. If you've got a game and an open spot, let me know.
* Read more blogs
  * I'm trying to replace my spare phone-time with carefully curated gaming-related stories via RSS from the blogosphere. I'm not making a lot of progress through the posts I've got to read, but I am slowly shifting what I'm doing when I'm wasting time. Manga's stronk tho.
  * The other thing I'm experiencing here is that I'm being overwhelmed with the OSR content, so I don't see many articles or posts about indie games, new design, or non-D&D content. If you know of any good ones about design or reviews, let me know.
* Listen to Podcasts
  * I just got a useful podcast app the other day, and I'm in the process of trying to curate a list of podcasts that are relevant to me. If you know of any good ones, again, let me know. My problem with podcasts & videos is that I can't pay attention to spoken word while writing or designing because it's too distracting.

Anyway, I'm sad that I don't know more stuff. I'm trying to listen though.