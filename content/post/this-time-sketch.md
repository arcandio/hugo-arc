---
title: "This Time Things Will Be Different"
date: 2020-07-23T11:18:34-05:00
tags: [art, sketch]
draft: false
featured_image: "/images/2020-07-23-11_19_17-Navigator.png"
hidehero: true
---

![A random ink sketch](/images/this-time-things-will-be-different.jpg)

just a random sketch trying out some new brushes.

no real meaning, just happened to be listening to NIN at the time. Stream of consciousness sketching. I always default to a female face I guess.

I think this is leading me in the direction of a possible art style for Fangtales?