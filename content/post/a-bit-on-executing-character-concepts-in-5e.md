---
title: "A Bit on Executing Character Concepts in 5e"
date: 2019-11-15T11:07:18-05:00
tags: [blog, tabletop-rpgs, dnd, game-design, rant]
draft: false
featured_image: ""
hidehero: true
---

*I feel a bit like I'm struggling upstream when trying to create some kinds of characters in fifth edition Dungeons & Dragons. Time for a bit of a rant about rules-enforced tropes.*

I understand. 3.5 had a problem with keeping skillchains under control due to the massive amount of not just 3rd party but 1st party content. It became very difficult (from approximately the very outset) to keep 3.5 under control, especially with skilled powergamers at the table.

I get that.

I get that 5th edition is designed with the intent to prevent some of that craziness, both in terms of sidelining multiclassing, but also in the design philosophy of bounded accuracy and limited magic item distribution.

But the thing that I have a bit of a problem with in 5th edition is that it seems like it seems somewhat harder to make decently optimized characters to a specification you have in your head. I say this because I recently spent a huge amount of time crafting a character for an upcoming game, effectively trying to "min/max" just the basic abilities the character should have: skills, attacks, spells, etc. I my primary concern wasn't really that the build wasn't good enough, it was more that when I was exploring the class options related to what I wanted to do, I was met mostly with either A: just no mechanical support at all, which would force me to either homebrew something or abandon the character concept, or B: a combination of effects (usually via multiclassing or dips) that got me close to the effect, but not even remotely effective in combat.

Here's the thing though: I don't think that any edition of D&D has ever really been that much better at articulating character concepts that well. Even 3.5 had fairly limited mechanics, in the grand scheme of things. It was just that there were so freaking many that you could often dig up some published source that had a class you could multiclass with to make a desired build happen.

And again, let me stress this isn't about making powerful characters. It's more about building the right character. I would bet that most players who've been around for a while have encountered situations where some thing that they want to be able to do either simply didn't exist or was so awkward to construct using the rules that it made it effectively useless.

# Classes

Classes are part of the problem here. A class is a self-contained package of traits and powers, but each of those packages only has so much room to customize. Sure you can reskin spells or effects. Sure you can pick paths and select your spells. But so often there are choices that aren't really choices because it'd be insane to take one thing over another if you wanted to be effective in the game.

Sidenote: I know a lot of people are going to rag on this outlook by saying that not everyone wants to play an optimized character, and I get that, but also consider that since this is a [role-playing] [game], that you shouldn't be forced to choose between roleplaying and gaming.

Why does D&D have classes at all? Here are the main 3 reasons I can see at the moment.

- Tradition: Not a huge fan of this argument, but conversely, a lot of people didn't like 4th edition because it broke from the traditions of past editions so much. On the other hand, "because we've always done it this way" is rarely a good reason for anything. Branding might be a stronger way of putting this, I suppose.
- Simplification: It might be an effort to minimize the overwhelming number of options you find in something like GURPS (which you have to admit is just one of the plain ugliest sounding words used for an RPG out there, I've just realized). The problem here is that it's not really that simple. No way and no how. There are plenty of systems that allow for customization that are far simpler. There are just too many subsystems to make that claim.
- Balance: Errr. Well, this is really the crux of the matter for me. I don't really like how unbalanced D&D can be. Granted it's less of a problem in 5 than it was in 3.5, by far, it seems, but there's still one glaring problem. None of the components, from racial abilities, to stats, to skills, to spells, to class features have any sort of valuation formula or construction rules. On one hand, that seems to be so that each class can play drastically differently than another, but on the other, it becomes almost impossible to really understand the power level of the party unless you just do all the math and calculate the total output of any given thing they can do, which is like using a quantum computer to simulate a quantum system: it's not a calculation so much as running the process to see what happens. So in the end, we just have to trust that the system "is balanced" somehow, because it's so hard to verify. Effectively, we take it on faith. (this isn't just a problem with balancing against the current party either. It gets even worse when trying to plan for the future, i.e. in prepping a session or planning a campaign.)

# ASI Lock-In

It's almost a truism at this point that a game has to have choices that both matter and aren't so obviously unbalanced that nobody ever takes one option over the other. But D&D has had a problem with this in terms of character creation from the very beginning. In early editions, you couldn't even play both an Elf who was a Fighter. Later on, you were penalized for thinking outside the box or limited to certain things that particular races were good at. I'm looking at 2nd Ed class level limitations per race. In 3.5, we got more progressive and just applied Ability Score penalties to things a particular race wasn't good at.

Here's the deal though. Say I want to play a half-orc wizard. He was the runt of the litter, the smart one. In 3.5 my orc was just going to be a bad wizard. At least until he got some ASIs and magic equipment, but that -2 Int was going to stick with him his whole life, compared to an Elf or Human. The moment you want to renegotiate that with the GM, you're off the reservation, firmly in the territory of "homebrew." Which, like, I mean, I've done a shit-fucking-ton of in my time as a player and DM, but why should I have to do that in the first place? Why not have rules that support interesting character concepts rather than make you invent new rules to do the things you want? It's not really good enough to allow people to change the rules, I want rules that actively help me in a positive way.

As an example, check out the chart here. Note how there are some class combinations that are just far and away more popular than others, by like an order of magnitude. That doesn't follow with the totals per class and race either. It's just that some combinations are good and some are bad. I wouldn't call that great game design exactly, and the direction each edition has taken in progress seems to indicate that the designers tend to agree with me. Also note that the implication there seems to be that making "rare" and thus "unoptimized" characters is somehow a better or braver or more hardcore choice. Calling it "rare" paints it as desirable, which stands at odds with the reality shown in the chart. Most players are trying to play a useful character.

# Some Ideas

- Liz and I talked about a more demographic-style system for stats. I still want to write that up and present it, but the plate's full right now. The idea would be that no race has any bonuses at all, instead, each race has a specific dice roll for each stat individually. Imagine for example, rolling 5d6 drop 2 for an orc's strength score. After some consideration, it might lead to a similar outcome, but at least you could possibly come up with more interesting characters that way.
- Using Point Buy doesn't alleviate the problem either, but if you got rid of the stat bonuses themselves and gave the PCs more points, maybe that would work.
- Allow bonus swapping. If a race says +2 Strength and +1 Constitution, allow the PCs to swap those around. Or just let them place the bonuses where they want so long as there's good backstory/reason for it. Thence cometh ye homebrew.
- Detect Balance and modify the stat blocks for races as needed. This is a good start to generating reasonable homebrew with more flexibility.
- D&DBeyond has really kind of exacerbated the issue: by presenting such an easy way to play & build with the standard content, and providing some obscure and kind of difficult methods for homebrewing, they've contributed to making minor changes to character components less popular. Conversely, there's a ton of homebrew on there to choose from, much of which is unbalanced, which also makes DMs gun-shy, which all comes back to D&D not having actual formula-based rules for constructing classes, spells, races, etc. Detect Balance is a great solution, but to only one of those sections. Ultimately, it'd be nice if WOTC provided us a Detect Balance for all the major components of the game.
- I've got a small niggling thought that I could design a parallel class and race system compatible with 5e that would require you to multiclass, either through having each class so limited that you have to manually reconstruct each class, or using a Detect-Balance-like point-based methodology for purchasing class abilities. On one hand, it sounds like a pretty good solution (if we could get it tested well enough) but on the other, I'd want to put a LOT of extra flavor content in there, thus possibly moving it from a generic, widely applicable book to a campaign setting type thing with less broad appeal.
- pick another system. Doan wanna.

If you've got anything else, let me know. I'd gladly add it to the article.