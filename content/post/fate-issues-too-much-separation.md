---
title: "Fate Issues: Too Much Separation between Player and Character"
date: 2019-03-14T07:24:48-05:00
tags: [fate-core, game-design, tabletop-rpgs, blog]
draft: false
featured_image: ""
hidehero: false
---

*A continuing series on the #CommonComplaints of [#FateCore](/tags/fate-core)*


This is one that I *do* agree with, in some ways. Fate Core treats the PCs like characters in a story that the players are telling, rather than the avatars of players in the world of the game.

[![A page from Oubliette Second Edition](https://i.imgur.com/WDzWBgv.png)](https://i.imgur.com/WDzWBgv.png)

In Oubliette Second Edition, I had a whole section on handling this disconnect. In that game, I specifically and intentionally backed off from that extreme player vs character separation.

On the other hand, I do also think that it's not difficult to role-play a character in Fate Core as written. The contention is that if you're role-playing *your character,* that you'll never knowingly do something that might hurt your own chances of success. This is patently, provably, hilariously false, not just in fiction (the [death seeker is a trope](https://tvtropes.org/pmwiki/pmwiki.php/Main/DeathSeeker) among others) but also in reality (addiction, suicide, etc). Thus I'd argue that it's actually easier to role-play flawed characters and take that offered fate point when the GM offers you a compel on your trouble, not just because that'll make the game more interesting and dramatic (the external view) but also because you know that your character's willpower might not be strong enough to reasonably refuse (the internal view). It hurts, but it hurts in a good way. It evokes pathos in the player for their own character.