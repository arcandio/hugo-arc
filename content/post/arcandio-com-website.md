---
title: "Arcandio.com Website"
date: 2019-10-12T8:37:18-05:00
tags: [programming, portfolio]
draft: true
featured_image: "/images/2021-10-12-08_52_14-Arcandio---Joe-Bush.png"
hidehero: false
tech: "Hugo SSG, Webdev"
description: "Personal blog and website"
year: 2019
---

# Description

A blog and CMS for my own personal portfolio.

## Business Purpose

## Structure


# Process

# Challenges

# Points of Interest

# Critiques

# What I learned

# Demo

