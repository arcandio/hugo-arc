---
title: "A Wild Podcast Appears"
date: 2021-01-20T11:07:50-05:00
tags: [blog, joes-table, podcast]
draft: false
featured_image: "/images/20210117_211650.jpg"
hidehero: false
---

[![Joe's Table Podcast Cover](/images/jt-podcast-cover-with-bg.png)](https://anchor.fm/joes-table)

> The Podcast branch of Joe's Table, where Liz and I talk about games, game design, art, creativity, problem solving, roleplaying, fantasy, fiction, tropes, technology, software design, and whatever else comes to mind!

# A Podcast? In *my* Arcandio? Why Yes!

As the blurb mentions, the idea here is pretty much for me and Liz to talk about creative topics that we feel passionately about. At the time of this writing, we've got 5 episodes completed and ready to go, with another 4 in the pipeline and in the process of being edited. 

# Recording

We're slowly dialing in on the optimum recording solution. I tried to use FFMPEG to record everthing so that we could have a Joe Rogan style video podcast with full multi-cam switching, but it turned out that the mics we're using (two identical Fifine USB mics) have slightly different recording speeds, so over an hour of recording the two channels desync, causing pretty bad echo. So instead, we're using OBS at the moment, and just bagging the video portion. That should make things easier to edit: We're recording a single video stream with three audio streams: One for OBS's realtime mix, one for Liz's track, and one for my track. We can use the single tracks to fix any audio problems during the recording, such as bumping a mic or one person talking over the other or whatever.

# Editing

So instead of using Audacity like everyone says, we're using Resolve. The neat thing about this is (aside from that we can make videos with it for Youtube podcasts) we can share the raw video between our computers and both be editing footage on our computers at the same time. And that's a huge load off my back, since Liz actally enjoys editing.

# Joe's Table vs Joe's Table

So what does this mean for Joe's Table Videos?

Well, probably some of the content I wanted to make videos about would be better as a podcast, but there are also topics I want to do more video-production type presentations for. It's hard to show off a character sheet for a game, for example, in a podcast. So there'll definitely be more Joe's Table videos.

Conversely, there will probably be a lot of Joe's Table Podcasts, because they're so much easier to edit and more fun to record, but a lot of that content wasn't ever going to be a Joe's Table video to begin with, so we're probably gaining a lot more content than we would have "lost."

# Release Date

Not sure yet. We're trying to get our ducks in a row for promotions, but I also feel like we will probably power into the promotion more as we have more content to promote. Not a lot of sense doing a huge launch for one episode. Probably better to promote more as more content becomes available, to a point.

# Schedule

We think weekly, right now. We can record 2-3 episodes on a Saturday afternoon and have them edited in a week or two if needed. We've got a backlog right now and will be trying to maintain a decent backlog going forward, so we might be off the "recent events" track, but at least we'll be consistent.

# Support

The podcast basically has to carry its own, since it's not producing RPG books for us. We don't anticipate doing a lot of marketing our own work on the podcast, except incidentally, so it needs to be it's own thing. For that reason, we'll be having a listener support donation system. If that don't work, we might try Patreon, but that's not been very useful for us in the past.

We're also considering Sponsorships, but we're unsure about that right now. I don't think it's our favorite thing, and often feels like it cheapens the output. If we do it, we'll try to do it as painlessly as we can.

# Distribution

We're using [Anchor.fm](https://anchor.fm/joes-table) as our host, which allows us to publish to the standard platforms like spotify, apple, google, etc. As I understand it, once we set it up to publish on a schedule, it'll just fire them out to all the podcast aggregators without our interference.

# Joe's Table Podcast II: Expansion Pack

We've got a lot of ideas for content to talk about, mostly around the [#TTRPG](/tags/tabletop-rpgs/) scene, gaming, and creativity, but we're also considering doing some interviews or talks with our friends. If you have an idea for a podcast or for a topic you'd like to hear us talk about, hit us up on [Discord](https://discord.gg/r2rPW5c). Another neat thing is that Anchor allows listeners to record their own audio messages to give back to the podcast creators, who can then in theory incorporate those messages into their podcasts. You shoul totally try that too, though we don't know how we'll be handling those yet. It'll probably depend on what ends up in the inbox.

Anyway, look forward to more chatting with Joe and Liz on the [Joe's Table Podcast](https://anchor.fm/joes-table). Look us up on the podcast host of your choice!