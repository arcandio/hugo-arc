---
title: "Oob Sketches"
date: 2020-08-05T10:06:08-05:00
tags: [oubliette, o2e, sketch, wip, art, blog]
draft: false
featured_image: "/images/2020-08-05-10_02_42-.png"
hidehero: false
---

Well, I finished all my recent [commissions](/page/commissions), so today I did some art for my ongoing Oubliette Second Edition game. One of these is an NPC the PCs are going to be up against eventually, the other they already know.

![thorny, androgynous wood elf half-ent Themis](/images/2020-08-05-10_01_51-Navigator.png)

I'd like to polish up this one, because it seems like it'd make a really interesting painting. Good movement, unusual character design, rule of thirds, etc.

![a magical girl of whooping ass](/images/2020-08-05-10_01_21-Navigator.png)

This one I'm somewhat less impressed with but it's not bad for my work, and it's not bad at all for the fact that I only spent like a half hour getting to this point. Maybe it'd look more pro if I cropped it in a ways.

It was fun to do some channel-point-sketches, and I think I should do those more often, possibly once a week or something.

I meant to get to one of the major villains of the story, but I didn't make it. Maybe tomorrow.
