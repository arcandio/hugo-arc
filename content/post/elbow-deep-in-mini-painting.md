---
title: "Elbow deep in mini painting"
date: 2022-06-06T9:50:18-05:00
tags: [blog, 3d printing, art, tabletop rpgs]
draft: false
featured_image: "images/PXL_20220530_165742845.jpg"
hidehero: false
---

Hello, blog. [Long time no see](https://arcandio.com/post/30-years-of-not-learning-guitar/).[^1]

# The Story So Far...

Wow, yeah, it's been a while. A lot has happened since my last post, and even the last few weren't really blog posts, so much as portfolio updates. As of the last update, I'd JUST started getting deep into mini sculpting and painting. Here's a fangslime from around then.

![a fangslime from just before then](/images/20210916_140909.jpg)

Now, I've been busting my ass for a couple of years on [SGS](https://voidspiral.com/product/sgs/sgs/) and I've still been grinding away at my "day job" in app development. Because of the pandemic, a bit of a lull in our local and remote games, and my weariness with work, roughly at my last post 8 months ago, I started doing mini painting in my easy chair in the evenings to relax. Which has largely been *absolutely fantastic.*

Here's roughly where I was at at that time in terms of skill. Taken October 2021.

![October 2021 mini painting examples](/images/20211024_083259.jpg)

At that time, I wasn't huge on blending, not great on contrast, and relied on a lot of pretty rough and ready techniques like bad drybrushing and whole-dip washes. Not that these minis are bad to look at, they're certainly better than the old 3.5e D&D WOTC plastic minis I have.

Around about that time[^2] I built a 2foot by 2 foot by 4 foot high painting caddy / workstation thing that I could roll around the living room and keep next to my chair.

![Stuff on my painting workbench](/images/20211212_213110.jpg)

That did *wonders* for my ability to keep things tidy and organized[^3]. It also gave me the ability to "save" projects. In addition to some laser-cut boxes I made and some simple dowel painting handles, I can now set things aside, gather the paints I need, and keep my work state in "memory" between painting sessions. With my new wet pallete from Red Grass Games, I can pick up and put down my painting almost as fast as I can clean the brush and set it down.

![My laser-cut painting handle holder, 1 of 4](/images/PXL_20220519_005346855.jpg)

Now I'm often painting between our afternoon walk and dinner, if I'm not helping prepare it, and after dinner while we watch some TV, as long as it's not too engaging[^4].

A lot of this has been driven by Liz and I getting Warcry--

> --holy shit. I just remembered we've got all the Gloomhaven minis I could paint too. Whoa. Sorry, continue.

--which is a blast, but requires a lot of new minis, as well as several new [#TTRPGs](/tags/tabletop-rpgs), where I need to paint PCs, NPCs, and Monsters.

So over the winter and up until now, I've been bingeing Youtube mini painters, buying ever more paints and bushes, and slowly working my way through a big fat backlog of primed minis. So far, my major projects were:

- Random D&D Minis, not goals or techniques
- Kobolds from Thingiverse, first attempts at glazing and Non Metallic Metals
- More D&D minis, more techniques
- Experiments with Hero Forge minis, some Object Source Lighting
- TTRPG minis for Westvault and Galerose
- Skaven for Warcry, edge highlighting, glazing, coverage, and weapon texture techniques
- Liz's Corvus Cabal minis, expansion on glazing, layering, FX paints
- Nurglings from Thingiverse, highly entertaining to paint
- Glaivewraiths and Myrmorn Banshees for Warcry[^5], awesome blends & cool weapon textures
- Chimera for the Noble Knight painting contest, first detailed large mini
- Chaos Knights for Warcry[^6]
- Demon Girl Bust, first bust, lots of huge blends, first big face
- Dragon Ogre from Thingiverse, just started

So like, that's a lot, right? I should probably have written blog posts about each project as I went, but hindsight[^8] and all that. Most of this stuff has been posted to my [instagram](https://www.instagram.com/arcandio/) so you can check a lot of them out there if you go back in time enough, but here are some highlights[^7].

![fangslime and fangslime big mama](/images/20211015_194322.jpg)
![demon lady from Hero Forge](/images/PXL_20220321_181749336.jpg)
![I like demon ladies, okay, sue me again](/images/PXL_20220321_182024166.jpg)
![Thingiverse nurglings damn gross I love them](/images/PXL_20220403_183032063.jpg)
![Wizkids Chimera for the Noble Knight painting contest I'm gonna not even place in](/images/PXL_20220517_124501284.jpg)


# A Word on my Mini Sources

I've painted minis from a lot of different sources now. Here are some thoughts.

- my own sculpts
    - relies on 3d printing quality
    - my sculpts are currently too realistic, meaning I need to make the models more cartoony with extreme proportions to make them read better on the table
- Thingiverse
    - relies on 3d printing quality
- Elegoo Mars 2 Pro
    - good, but leaves a lot of cleanup on some sides.
    - leaves layer lines on flat spots facing the wrong way.
    - definitely has some "tooth" to it, so models aren't perfectly smooth finish like plastic kits.
- Games Workshop Plastic
    - ugh buttery smooth and perfect in every way
- Games Workshop Metal
    - joins are fragile, maybe it's just my glues.
- Wizkids Primed
    - mold lines are SUCH a pain in the ass with these, you end up needing to really sand down the priming after mucking it up.
- Siocast
    - never tried it, but really wanna

# A Rundown of Techniques

- Glazing
    - use it ALL THE TIME.
    - Vallejo Glaze Medium Baby Yeah.
    - Two-brush blend them edges.
- Layering
    - I don't layer much, my layers are mostly like glazes instead.
- Two-brush blending
    - I use this a LOT with blending now, apply color, clean brush, blend edge with water.
- Wet Blending
    - I don't do it with paints alone.
    - I wet blend two pure colors over the top of a coat of retarder, which gives me more working time.
    - works good for large areas on complicated parts.
    - seems to work best as a lower layer.
- Washes
    - use em all the time.
    - Getting better at precise application, see the Chaos Warriors' shields at the end of the post.
- Airbrush
    - use it all the time for basecoating and varnishing.
    - Never used it for blending surface colors on a model yet, never had a use case.
- Non Metallic metals
    - I'm not very good at this yet, probably owing to bad blending.
    - very hard to do on tiny surface areas.
- Object Source Lighting
    - very cool, but I should probably practice this on smaller areas instead of on huge whole models.
- Effects Paints
    - fun, but hard to decide whether I want to go the Effects/Metallics/Glossiness route, or the NMM/OSL/Matte route.
    - Some paints, like Turbo Dork irridescents look neat even with a matte finish over them, like crow feathers on Corvus Cabal.

# Side Quests

I mean, it was like 8 months, a lot of other shit went down.

## Terrain

I made a bunch of terrain over this period too.

![Building steampunk ttrpg terrain WIP](/images/20220216_073738.jpg)
![steampunk terrain complete](/images/PXL_20220319_231000487.jpg)

## New D&D Table

Apparently I never even did a blog post about the new D&D table that I built myself. It took a good deal of time and labor, but it was relatively inexpensive in the long run, so that's good Bout $150 in wood and tools. Hella sturdy.

![work in progress](/images/20211215_131324.jpg)
![table complete](/images/20220128_172611.jpg)

## Mini Photo Booth

I worked really hard over many hours to try to figure out how to take good pics of my mini paint jobs and 3d prints. Some of this comes from the fact that I put a bunch of my minis on Etsy, so I needed to get good photography of them. The other part is for Instagram. I tried about 5 different professional cameras, several phone cameras, and even some webcams. I set up a little backdrop over my desk, complete with wide area LED lights. I set up a little diorama made up of ANCIENT dungeon tiles for MageKnight or something. It's getting better. I still have a white backdrop which doesn't work so good for the photos (the cameras want to expose so the white is gray, etc) so I'm trying to replace that.

## Streaming Painting

I tried to stream some mini painting. At first, I did some prep with Liz on the new D&D table using a mic arm to position the camera, and that worked well, but I'm not at the table when I'm painting. So I built a new old PC rig and put it behind my chair and wired it up with webcams and a tiny monitor so I could watch for chat. Nothing really happened, so I sort of forgot to keep doing it. Then I took the equipment down. It's just really hard to get good video of that kind of thing. I'd need a very specific camera for this I think. I'd have to have...

- long depth of field to accomodate me moving the mini around
- short minimum focal depth to get close to the mini
- high magnification to fill the screen with the mini
- autofocus that aims for the CLOSEST thing rather than the farthest

Not sure it's worth it. If I could make it so that it's fire and forget, then maybe, but probably not at this point.

## New Phone

While trying to find the right camera and figuring out how to stream, I decided the best solution to both was a new phone with a really great camera, which I got. A lot of the photos in this post were taken with it.

## Liz the Magpie

I helped Liz set up her own jewelry shop on Etsy, check her out [here](http://LizTheMagpie.com)!

## The De-expanding Woodshop

https://arcandio.com/post/the-expanding-woodshop/ but inverse. We got a second car (prius again yay) and that means that the garage is REALLY tight for any woodworking tools. So I've mostly moved all the usual suspects to the basement, where I've got things set up by the 3d Printers.

## Game Dev Projects

I've also been busting ass on a bunch of [SGS](https://voidspiral.com/product/sgs/sgs/) games. Specifically

- The Wytchheart Machine, Steampunk/dieselpunk TTRPG
    - Galerose campaign Setting
    - Westvault campaign setting
- DisOmnia, To be renamed, tactical miniatures game
- Spiral Game System Tutorial Videos (on hold for now)


# Finally, some more minis


![A unit of Chaos Warriors](/images/PXL_20220530_165742845.jpg)


[^1]: It's only been like 8 months, after all. Yeesh.
[^2]: I think, don't quote me on that.
[^3]: And yet, now I have a pile of plastic tubs spilling out in front of it anyway. Time to clean more.
[^4]: Star Trek TNG is good for this. <br /> ![wub wub](https://media.giphy.com/media/8XNDS87OUrir6/giphy.gif)
[^5]: possibly some of the dumbest choices for a Death warband, great job dumb*ASS* <br >I'm sorry ok, they're cool as shit, sue me
[^6]: God damn it, I wanted the SLAVES TO DARKNESS BOX, I got these NORMAL minis, not the fucking awesome new sculpts DAMN it why am I so bad at this, khorne help me
[^7]: lammo. geddit? highlights? It's a pun! Because we're talking about painting! And you do highlights! When you're painting! HA
[^8]: It's how you see hineys. 