---
title: "Heroines of the First Age Writing Portfolio"
date: 2024-04-29T12:07:18-05:00
tags: [self, writing, portfolio]
draft: false
featured_image: "images/hfa-books.JPG"
hidehero: false
---

This page has a few little snippets of writing from Heroines of the First Age. They're not necessarily in order, so take each section separately.

> If you're looking to hire my services for writing, please contact me and I'll gladly send you any of my games to read as portfolio material.

Heroines of the First Age is set up differently from a lot of other RPG books, in that a lot of it helps the GM and the players construct the world together, instead of providing pre-generated content. Much of the writing involved creating evocative lists for people to pick from.

---

# Creature Portfolios
- The Benthic Ones. From the depths of the sea they come to 
gorge on the bounty of the land.
- Chthonics. The unfinished peoples of the origin of the universe 
want revenge on our perfected forms.
- The Corruption. Spore spreads. Such is the prerogative of this 
creeping fungoid danger.
- The Forsworn. Take care what you swear and what you vow, 
because those promises remember your words.
- Gigas Army. These mountain-vast titans can wipe clean the 
lands of their enemies quite literally.
- The Gilded. Are they empty suits of armor and cloth-of-gold, or 
is there spirit inside them?
- Paperlings. Enemies of the written word, these manifold beings 
seek to destroy all language.
- Plagueborn. These reconstructed zombies seek 
to dissolve all life into a single enormous world-
sized biomass.
- Megabeasts. Fauna so enlarged that they 
scarcely notice lesser lives, they hunger for 
ever-greater meals.
- Pillagers. These bandits and thieves are 
only loosely tied together into hordes that 
attack civilized lands.
- Sky Things. They come from the far side 
of the sky, and their hideously alien forms 
are nothing but nightmare to us.


# Rising Villains
- Tethyphar, The Matriarch of the Dark Pride
- Rungraeyu, The Inventor of Many Evils
- Qlogh, the Mad Necrophage
- Akhethaqt, the Conquering Empress
- Valarua, the One that Takes in the Fog
- Dhaoth, the Phobomancer
- Thaygon who bleeds the youth
- Asmarith the Thousand Tailed
- Izobram, Stalker in the Dark
- Samog the Slaver King
- Raza, Dancer in Flames

# Looming Prophecies
Some prophecies are of vital importance. They can foresee the rise 
of great powers and the fall of nations.

- This world will soon be awash in blood and all shall fall, making 
way for the next. How will they remember us?
- When the obelisk is completed, time will forget all that is not 
engraved on it.
- Weakening of the blood has cut our time short. The next genera-
tion will not survive.
- Disaster will come to all tribes equally, except the chosen ones.
- All of history will echo with the oncoming battle.
- As soon as all the nations are one, all shall fall.
- The Great Gathering Dark has finished its preparations. The tides 
now come crashing down.
- The hero everyone heralds is a false messiah. The true champion 
hunkers silently in the shadow, chained like a dog, in this affront to 
justice.
Intervening Gods
The Heroines of the First Age are often beset by vengeful and angry 
gods. Drawing their powers from the Pools, these beings assert 

# Intervening Gods
The Heroines of the First Age are often beset by vengeful and angry 
gods. Drawing their powers from the Pools, these beings assert 
their power on the world. There may be other gods who do not 
cause such terrible violence, but they make poor Immediate Threats 
and are better suited to worship by the religions of the world.
Also make sure to ask what exactly the god is interfering with, and 
how their actions can theoretically be mitigated by the characters.

- Atemereb, The Babbler
- Crow, the Blood Drinker
- Gezaap, The Vision of Death
- The Orphanides, the Infernal Goddesses
- Tanemis, the Roving Demon City
- Vuewa, God of the Songs and Lies
- Heldor, Who Sunders What is Wrought
- Aelgrim, God of War and Vengeance
- Kwatan-Mog, the Cursed God
- Orazoth, the Egg Eater


# GM Section
## Journey
> What lies beyond? Over the next hill, or over the one after that? 
>
> Fortune or death? Fame or obscurity?
>
> How will I know if I wait for the sun to set on another wasted 
opportunity?
>
> To the victor, the spoils.

There are many pitfalls along the way, but this section will guide 
you. Here you will learn the various ins and outs you need to make 
your way in the world.

## Fiction-Driven

Heroines of the First Age is a conversation. The GM presents the 
players with situations, environments, problems, characters, ene-
mies, and challenges, and the players choose the paths that their 
characters take through the world.

The rules and the story support each other. Much of the game time 
may be taken up by activities that don’t even call for dice rolls, 
which is fine. Other times, the rules will be ambiguous. In these 
situations, go with what makes sense in the context of the story. 
You may also find situations where the rules allow something that 
doesn’t make sense in the fiction; go with what makes sense.

The story itself drives much of the rules in the game. The fiction 
determines whether the Aura tag on a Weapon is important in the 
scene. The fiction determines what the environment is like and 
whether it helps or hampers the characters. In matters of the Fic-
tion outside the realm of the PCs, the GM is the final arbiter.


## Danger-Limited, Not Time-Limited

Another important aspect of the game being fiction-driven is that 
actions are not time-limited the way they are in turn-based games. 
Turn-based games generally give the PCs a limited amount of stuff 
they can do in a given period. Heroines of the First Age is different. 
Instead, it limits the amount of stuff the PCs can do with danger 
and risk. If a PC asks how far they can run, the answer is likely to 
be as far as they want, so long as there’s nothing to stop them. But 
if there is something to stop them, they’ll be limited by that factor 
and their success or failure to overcome that obstacle.

## Flow

Heroines of the First Age is not like many role-playing games that 
rely on turns, rounds, or units of time to determine who can act 
when. It is fluid and dynamic. It is a complex conversation between 
the PCs and the GM.

Usually, the game will flow as a series of exchanges between the 
GM and the players, with the GM presenting some challenge or 
problem and one or more of the PCs stepping up to propose an 
action. The actions are then resolved, either using Moves or simple 
agreement (in the case of simple or unopposed actions). The GM 
describes the effects and prompts the players again and the cycle 
repeats.

# Creature Portfolio
## The Benthic Ones

**Motivation**: Feed on the Abundance of the Land

**Moves**: Bring the Flood, Entangle a Foe

The Benthic Ones are creatures from the depths of the sea. They 
often have hard shells, pale forms, worm-like appendages, feelers, 
tentacles, and usually lack eyes.

Benthic Ones on the surface tend to gorge on anything they can be-
fore returning to the depths; food here is far more abundant than it 
is in their watery homes. Their comings and goings often coincide 
with storms and hurricanes, and their arrival brings tidal waves and 
flooding.

- Weak: hardshelled snailshrimp
- Tough: humanoid hermit crab
- Powerful: worm-headed mega-crab
- Great: armored land whale
- God: benthic trench crown-city

# Power Pool Creatures
## The Ink

**Portfolio**: Writing, knowledge, communication, immortality.

**Motivation**: Cultivate thought and absorb knowledge

**Moves**: Force their Flaws to the Forefront, Hide Among Innocent 
Thoughts, Grant Great Mental Faculties

Creatures of the Ink are made from the thoughts and desires and 
crimes of civilized folk. They are curses, fears, and desires, given 
form. Their black and purple bodies are composed of inky charac-
ters and forbidden words, and their powers are drawn from word.
The Ink wants to absorb knowledge, and it’s more than smart 
enough to seduce even the most righteous priests and sorceresses. 
It can live easily inside anyone harboring great intellect.

- Weak: floating glyph-scout
- Tough: curse-faced phantom
- Powerful: word-shifting ghost of passion
- Great: walking library of curses and maledictions
- God: corpus esoterica of forbidden knowledge