---
title: "In Defense of Black Bases"
date: 2019-12-11T11:14:19-05:00
tags: [art, minis, tabletop-rpgs, blog]
draft: false
featured_image: ""
hidehero: true
---

*Unpopular opinion time!*

I like black bases on miniatures.

I *like* them.

A well-based mini can be a fine thing, but in my humble opinion, they tend to drown out the mini. Good art is about **contrast**. I want my minis to stand out on the table. I want them to scream "I'm the damn hero here!" Or villain. Or whatever. They don't do that much if there's a ton of hyper-realistic detail, bright edge highlighting, heavy texture, and saturated colors. You'd think some people like the ground more than they like their mini. Let the mini do its thing.

Now, I'm talking about D&D minis, and I think this is sort of a divide between D&D and wargaming. In wargaming, there are a lot of models, and individual minis are usually not emphasized. If they are, they're enormous. Many of those are whole scenes, with a rocky outcropping or skull or whatever for the heroic mini to stand on top of. 

But in D&D, I want clarity and contrast. I don't want anyone to get confused thinking that a character's standing in water because their base has water effects. I don't want a mini standing on the terrazzo tile an enormous basilica of  to have rocks under his feet just because. Black works because it acts as a natural shadow, and it pops the figure off the background that is the terrain itself. I *like* that.