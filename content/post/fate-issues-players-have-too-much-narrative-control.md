---
title: "Fate Issues: Players Have Too Much Narrative Control"
date: 2019-03-07T11:58:33-05:00
tags: [blog, tabletop-rpgs, fate-core, game-design]
draft: false
featured_image: ""
hidehero: true
---

*A continuing series on the #CommonComplaints of [#FateCore](/tags/fate-core/)*

Now that we've seen [how the core mechanic works](/post/fate-issues-complex-resolution-mechanic/), you can see that the PCs *do indeed* have a lot of control over their own success. What this argument fails to take into account is that the GM's job is to provide *the right* resistance to the PCs. If you want something to be genuinely hard for them, you've got to consider *their* abilities in your calculation.

**That's the definition of game theory.**

So if you want to present a challenge that really drains the PCs, give them something that would be patently impossible without several invokes, don't just assume that your can make decent TNs based on their skill bonuses alone. You, as the GM have a lot of tools at your disposal to increase the difficulty, from just narrating something to be harder (The door isn't locked, it's barred and *welded shut.*) to compelling PC aspects (how you gonna knock the door down with a broke-ass arm?) to invoking other aspects (also, the bad guys are coming down the hall now).

Another interpretation of this problem is that Declare a Story Detail allows the PCs to "override" the GM's narration at any time. While this is *sort of* true, it's also important to remember that Fate Core isn't designed to handle pre-made adventures, whether they're your own invention or from a book. It's a more flexible beast, and it wants you to follow the PCs, rather than prognosticate from a lectern. If any one single element is so *vitally important* that the PCs out-maneuvering it defeats you, then you're likely to have that same problem in any other setting. Players are experts at finding ways around your plot, after all.

A potential problem I can see but have never actually experienced in practice is the use of a DaSD to change or add something to an NPC that fundamentally screws up the NPC's threat to the PCs. I'm sure it's possible, but that goes back to being a flexible GM and being able to roll with it. Another way to handle this sort of thing is to only reveal characters and information as the plot demands, so that if you *do* have a scheme in mind, there's nothing *in play* for the PCs to screw with. If they're in danger of finding something out that would let them get too deep, put some interesting obstacles in their way to uncovering that info.