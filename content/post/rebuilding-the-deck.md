---
title: "Rebuilding the Deck"
date: 2020-06-05T09:27:21-05:00
tags: [blog, woodworking, blender, home-improvement]
draft: false
featured_image: "/images/20200527_175107.jpg"
hidehero: false
---

My hands are really sore right now, because I've been working on rebuilding my deck and porch for two weeks straight. For the 7 days before yesterday, it was morning to night.

Let me remind you that I'm a desk jockey. I'm not in shape enough for this not to faze me. I've demolished and moved about 1.5 tons of rotten garbage wood into the dumpster, several hundred pounds of bricks and pavers, and toted around another 1.5 tons of replacement wood. So my hands are sore.

But let's look at the progress so far. I haven't taken *a lot* of pictures of this process, but I have enough to give you an idea of what I've been up to.

![old deck, cut up](/images/20200525_140104.jpg)

The old deck. 35 years old, rotted out because the builder didn't leave any gaps between the boards for rain to drain. Before this we replaced a few boards here and there, but we still kept almost punching through.

The first few cuts here were with a circular saw to give me some space to start taking up boards. I hit them with the crowbar to break the grip of the nails holding them down, since the nails have been painted over many, many times and weren't accessible.

![dumpster](/images/20200527_175107.jpg)

We thought about hauling away the garbage ourselves with my parent's truck, but I quickly realized that would have been a nightmare, so I ordered a dumpster. I then realized how much money we were going to sink into this deal.

The wood here is mostly from the railings and front porch. I'm lucky I went with a bigger one and got one bigger than I asked for. We ended up almost filling the whole 12 yards.

![rare wild joey, dismantling the wall](/images/20200523_121618.jpg)

Here I am doing some of the very first demo on the porch, the stuff that was in the dumpster above. That cordless impact driver was the MVP of the whole deal; I'd bought it a while back for the express purpose of this project.

![deck stringers stripped naked](/images/20200529_182929.jpg)

In this picture we're almost done ripping up the back deck boards. I thought that we'd be able to keep those stringers. We didn't. And that cost us a *lot* of time. We spent 2 solid days with 4 people working out how to rebuild that understructure. There were a lot of stupid mistakes in it, including shims between the posts and the joists made from endgrain that crumbled to dust in our hands.


![several days later, deck boards](/images/20200603_182540.jpg)

This photo is several days later, after a huge amount of work rebuilding the framing and laying down decking. It's not done, but it's usable and safer than nothing. From here out we switched to working on the porch, so we could get that safe enough for the postal workers to get to the mailbox.

![lots of marks on the porch step](/images/20200524_201422.jpg)

The porch is *all* jacked up. Literally and figuratively. After we ripped out all the rotten old porch wood, we found a couple of concrete steps that weren't even *remotely* level or square. So we took measurements on just about every corner of every piece and I rebuild the site in [Blender](https://blender.org), because that's what I'm fastest in. With my wife's help[^1], we got this measured and I got it designed up in 2 evenings after work.

![blender porch design](/images/front-porch-blender-design.png)

Of course, having not worked with dimensional lumber in a while, I designed the entire thing wrong first, then had to rebuild it again using correct lumber dimensions, which turned out for the best because I also simplified the design substantially.

You can see that the driveway slopes quite a bit, so I had to figure a way to build a huge wedge to prop the entire outer edge up, one better than just shimming the hell out of floating joists under the deck boards, the way it was before.

![porch framing](/images/20200603_182533.jpg)

This all went together in 1 day of about 90&deg; heat. We had a sunshade up for a while and pointed my big blower fan up into it to keep it cool enough to work.

We had a lot of "fun" figuring out how to reattach that ledgerboard, because the holes in the concrete were pretty terrible, but my folks found some very clever pull-wedge concrete anchors for it. 

![porch deckboards](/images/20200604_195619.jpg)

And here we are as of today. I'll update the post with more when we finish it, but last night we at least got it to be safe enough to walk on.

Looks really nice out there too now. We're planning to put facing boards on the sides of it, but as it exists right now it looks alright.

# What I tried to do

Because I've fiddled with replacing stuff, and because we've looked at how it's put together, I knew that there were a fair amount of bad decisions built into this thing, as there seem to be around a lot of portions of our beautiful house. So I tried very hard to avoid making similar decisions.

* We used screws to attach things so that we could easily disassemble and replace anything we need to.
* We made sure that the structure is separate from the decking. Nothing is overlapping or built on top of decking that might need to be replaced.
* There's exactly *one place* where a deck board is screwed into another deck board, and that's the front facing board on the porch.
* I left notes to the future owner about how to disassemble some parts, where it wasn't obvious.
* I planned for maintenance and the replacement of parts and boards by making it as easy as possible to access or remove each bit.

# What I learned

* Every project requires multiple trips to the hardware store, no matter how prepared you are.
* Think it through first. Measure twice, then check it again before you commit. Sneak up on cuts if you're unsure, tired, or lazy.
* You never know what you'll find, or how many animal skeletons you'll exhume.
* Don't drop your box of nails under the deck and leave it for the next guy to clean up in 3 decades. Please.
* There was no way I was going to use my computer for anything useful in the 2-4 hours between stopping work and passing out.
* Don't trust the last guy ot have done it right. And be nice to the next guy.

# Things I really liked working with

I've always liked woodworking, but there were some real MVPs of this project.

* A standard, $10 **wrecking bar** and a movable board was my weapon of choice for pulling up the deck boards on everything. It also did most of the work of the structure demolition.
* My cheapo blue **speedsquare** / roofer's square was so useful I should have had one for everyone.
* Obviously the **miter saw** got a workout.
* The **impact driver** was *magnificent.*
* I got a new **table saw** for rip-cutting bits to fit in spaces tighter than the boards could work, and it works *way* better than trying to use a circular saw freehand against a straight edge.
* My wife and my parents

[^1]: She also wrote a ton of warnings on the steps for the mail carriers, but they washed off in the rain that very night.