---
title: "The Chosen One"
date: 2021-01-21T08:10:18-05:00
tags: [blog, tabletop-rpgs, dnd, game-design]
draft: false
featured_image: "images/20210121_200028.jpg"
hidehero: false
---

*An optional game mod for D&D 5e, in which you run exactly 1 character, but can deal with a normal party's worth of encounter.*



# y tho

Some of us folks play [#ttrpgs](/tags/tabletop-rpgs) one on one, for example with our significant otters.

# Overview

Pick 4 classes and build a character who has the abilities from all 4.

# Lore

You're ✨***The Fucking Chosen One***✨. That probably has some consequences for the way the story will be structured.

You'll also want to figure out *what* chose you. There are 3 basic directions here. As with everything else here, it'll be up to both you and the DM to figure out how best this'll work.

- chosen by a single god
- chosen by multiple gods
- chosen by the cosmos

Try to make sure the classes make sense for the thing that chose you. Don't build a conquest paladin for a god of peace and nature. use ya 🧠.


# Rules

- Pick 4 classes
	- all classes level up at the same time
- One set of stats
	- roll 4 stat brackets, or otherwise optimize the stats
- One set of skills
	- First, figure out what skills are available to you from all 4 classes.
	- Second, total up how many skill proficiencies you get for all 4 classes.
	- Pick ya skills.
- All class abilities
	- Basically as per the gestalt rules from 3.5
	- You get the best version of any given overlapped feature.
	- Spells are all tallied separately, and you'll have separate saves & attack bonuses for each spellcasting class
- All ASIs
	- yes, you get to pick 4 ASIs every 4 levels, lol.
		- your DM may want to decide that it's ok for you to break the 20 glass ceiling.
	- feats also viable as replacements, but be careful of making things to complex
- One background[^1]
- All proficiencies for each class
- One sub-turn per class
	- You pick the order of the turns
	- You can only use actions from that class on that turn
	- includes movement[^2] & bonus actions
	- preserves action economy
- All equipment, but sell anything you don't need or want
- Build encounters & select mons as usual

# Tips

- use color-coded turn tokens
- Avoid mixed-type paths, like Eldritch Knight & Arcane Trickster, because they make things more complicated.
	- instead, try to find 4 focused classes.
- Use a notebook. Write your own CS. Draw lines with pen, write stats with pencil. Stay organized.
	- Write down your actions by action type.
- Write out plausile combos ahead of time
- figure out all the ins and outs of this shit ahead of time. Don't wait for later to have an argument![^3]

# Notes

*will be edited as our test campaign goes*

- The action economy isn't exactly preserved: if the single Chosen One takes enough damage to KO the weakest class, they don't actually lose those actions the way the party would with 4 PCs.
- Some hysterical combos are possible, and encouraged.
- Taking rogue is a riot. Move Move Move Move, bonus action to dash. Bye yall.
- You'll probably find that some combos don't *quite* work the way you'd want. You can't Smite+Sneak Attack, for example, because those both happen on separate sub-turns.[^4]
- Also remember that some things have extraneous limits on them: Sneak Attack for example can ONLY happen once per turn, for example.

# Um

So I don't really know what else to say about this thing yet. We haven't run it much, and we'll probably revise this post when we encounter more problems, but for now, I guess, like, try it?

And tell us how it went on [Discord](https://discord.gg/r2rPW5c)

![spiffy custom sheet](/images/20210121_195943.jpg)

[^1]: who needs more than one? you already get pretty much all the fucking skills. What, do you need more than one proficiency in land vehicles??
[^2]: This is hilarious and makes you feel like The Flash
[^3]: have one now!
[^4]: Your game tho, aint judgin