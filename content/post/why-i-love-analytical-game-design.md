---
title: "Why I Love Analytical Game Design"
date: 2019-02-21T10:47:05-05:00
tags: [blog, tabletop-rpgs, game-design]
draft: false
featured_image: ""
hidehero: true
---

*And why I don't get to use it often.*

> Buckle in, this is going to be a long one.

## What's Analytical Game Design?

Analytical game design is just what I call doing a particular style of [#gamedesign](/tags/game-design) which mostly revolves around broadly-applicable design rules and the use of spreadsheets and data as the driving methods for design.

Fundamentally, I work in one of two general ways:

* non-analytical (as I did in Heroines of the First Age, Oubliette first edition, Wasuremonogatari, and others)
* analytical (as I used to do with old role-playing games, especially with #SilCore and many of my pre-Voidspiral, unpublished game designs)

Non-analytical design is sort of "freeform" and isn't usually focused on analyzing the mathematical space of the game design. It encompasses all the other ways to design a game apart from this analytical way I'm about to describe.

In analytical game design, rather than starting from special things I want to have in the game, like specific powers, classes, or races, I start with abstract mechanics, the base-level math, if you will. Usually this starts with defining the main resolution mechanic, then branching out from there to explore what the limits of the system are. As I find those limits, I can either expand the system (since there's not much content to change yet) or I can embrace those limits and codify or analyze them.

## A bit of history

For a long time as a kid, I played D&D exclusively. We homebrewed a *lot* of stuff. I designed worlds around campaigns and campaigns around stories and it was all completely unbalanced hackery. Which was fine as a player and as a GM at the time, as we weren't overly concerned with the "balance" of our homebrewed games.

I think I got the Heavy Gear video game before I got the book, but I actually have no idea. I do know that I got some of my #SilCore books from a book store in Arizona, where I spent a lot of my childhood summers, and some from a now-extinct book store in the Bayshore Mall in Milwaukee. At any rate, collecting an using Dream Pod 9's SilCore system quickly became an obsession for me, and I seem to remember that I was working on detailed spreadsheets for vehicle design and character creation in lotus notes, before I even got to college.

Let me just fanboy about SilCore for a minute. SilCore is freaking amazing. Like, it completely changed how I thought about game systems. Some of it's features:

* The system is designed to allow you to build an army of gears (mechs) from the ground up, and have it be balanced for strategic wargaming scenarios. Balance, therefore, is paramount in the system, because it assumes that players will use the same rules against each other.
* The system's effectively metric.
* Stat levels have specific values associated with them, making it possible to mathematically gauge the value of one construct against another
* The book presents rules for designing vehicles from scratch, in enough detail that it's possible to design creatures, monsters, powerful bosses, androids, mechs, tanks, landships, battleships, space ships, asteroids, planets, and stars, if you have a powerful enough calculator.
* The book presents rules for making subsystems as well, like weapons, features, and so on, adding to your ability to build effectively anything.
* Accuracy is integral to the damage calculation: `Damage = Weapon Damage x Margin of Success` which does take a bit more mental math to do, but provides a spectacular way of handling large, slow attacks vs small, precise attacks.
* Maneuverability was likewise integral to determining something's ability to evade damage.
* Therefore the attack system could be summarized like so: `Accuracy - Evasion = MOS. MOS x Weapon Damage = Total Damage.` So unlike in D&D, there was a very real balancing act between having good on-hit damage and being accurate enough to do that damage. And on top of that, it made absolute sense that the more accurate your attack, the more devastating it would be.
* Scale was also very important. Mostly the difference between personal-scale and vehicle scale, but we could extrapolate that out to ship-scale or space-scale, or whatever.

> That's enough Joe. Put SilCore down for a minute and get back to the matter at hand.

So I liked SilCore, and it taught me a lot about game design. Unfortunately, I never really figured out how to weaponize that deliciously elegant system into a game of my own, owing to the lack of an OGL or similar license. I even contacted the company and (I think it was even the head honcho) told me to go right ahead with designing a game *based* on SilCore so long as people still needed the Dream Pod 9 books to run it. It just never got off the ground.

I continued to use SilCore for a lot of my homebrewed games through undergrad, adding more layers and books to my style and spreadsheets. When I eventually got to the "game design" course in grad school, I was delighted to learn that it revolved mostly around spreadsheets and using them to help define and analyze the rules of the game. I'm not bragging, but I aced the hell out of that class because I'd already been doing that exact thing for years already. Additionally, my background in SilCore hacking contributed to me getting to be the lead game designer on two of the three major studio projects we did in gradschool.

But enough love story. SilCore was my first major foray into analytical game design, and also my first major foray into learning the internal parts of a design system. SilCore is really the fundamental philosophy of AGD for me. Let's get back to that, starting with some of the reasons I like working that way.

## Why I like analytical game design

* It's a "smallest piece" kind of approach. You break down each area of the system into its constituent pieces to define them. Even if the players of the game don't see that work, they can see the consistency it provides.
* It's effect-based. Instead of "making it up" you're induced to "build" with the pieces you've defined. You may need to define a few more pieces to make something work, but you've already got precedents for what they should do and how much they're worth.
* It assigns value to components in a meaningful way. Unlike D&D, where a given feat or spell or feature can have spectacularly different value than another element of the same type and level, AGD allows you to build elements with well-defined traits.
* It's "rational" rather than touchy-feely. Perhaps a better word would be "empirical." Because AGD is so based in spreadsheets and math, you spend time designing and redesigning entire suites of abilities, which you can analyze at every step to see if they're actually producing the effect you want.
* It's a closed system that feels like a sandbox. In D&D, it's beyond easy to pick up a new book that completely throws the balance of the game out the window. That's because it's an open system, where new content adds new mechanics that interact with old mechanics in novel (and unexpectedly exploitable) ways. (Don't get me wrong, the other thing I loved as much as SilCore was D&D 3.5, with all its powergameability and hackability) But a closed system AGD approach is less like D&D and more like physics: simple rules that are combined to give rise to complexity. That means you can invent as much new content as you want, and as long as it follows the same fundamental laws, it'll fit right in.
* I like spreadsheets

## Spreadsheet Design

I'm sure I've already lost a bunch of you to the idea of designing a **game** using something so boring as a *spreadsheet,* but stick with me for a minute. Spreadsheets are awesome. If you categorically hate math, game design of any kind is going to be kind of hard. I hated math a lot as a youngun, but I came to love it through spreadsheets.

[![An in progress weapon design spreadsheet](https://i.imgur.com/FBnBpeu.png)](https://i.imgur.com/FBnBpeu.png)

Spreadsheets force you to think about game design in a kind of object-oriented way, the way a lot of programmers think. It becomes necessary to make classes of objects, assign those objects traits and attributes, and then spend hours (days, months, years) creating instances of those objects. But critically, because you've codified those objects and their properties, you're imposing useful design limitations on your work. It becomes a conscious choice to break from the established rules, and those breaks also have to be codified. Soon you've got a very flexible platform *on top of which* you get to design the specific content of your game.

## It leads to iterative design

Changing the system, I freely admit (and will talk about later) isn't easy. But what *is* easy is making small adjustments. In a well-defined AGD, you can make small changes to certain parts which have sweeping effects across the whole system. Change the baseline weapon damage, for example, and that could filter down into average damage calculations and total hit point allocations. On the other hand, you can change a single instance of something without worrying about forgetting to update something somewhere, which means you're less tied to a single monolithic version of your game; instead you can jump in and adjust things as issues arise.

## It makes you focus on global mechanics

I've found that a useful side-effect of AGD is that my resolution mechanics are more sturdy and well-planned out when I actually employ AGD principles.

[![A diagram of basic resolution mechanics](https://i.imgur.com/tcn9mrW.png)](https://i.imgur.com/tcn9mrW.png)

Since you have to intimately know your core mechanics to build out the rest of your data in AGD, you naturally put those poor bastards through the wringer, because you're always using them as the basis for comparison of other, higher-level elements.

The downside of this is that you may spend so much time working on mechanics that are globally applied that the system can end up feeling too "generic," because all the rules stem from the same source "laws." Giving yourself enough tools to build outwards really helps with this, as we learned on Oubliette Second Edition.

## It's highly structured & regimented

It's much harder to lose track of small components this way, because you're not generally writing them in one huge monolithic word doc, you're designing component-by-component. You're unlikely to insert throwaway optional rules into the middle of the text, because you want to see how those rules play with everything else, so you likely end up putting in a new page in the spreadsheet for optional rules, or at the very least running a bit of analysis on them to see what their effects will be.

## It's more easily testable/analyzable

Because each element is broken down into its constituent pieces, and you have statistical tools to analyze the pieces and the elements they make up, you can pretty easily set up queries and lists for obvious issues like "most powerful weapon combination" or "chained abilities" or whatever. You can look at the outliers of a group of elements and decide whether they need to be knocked back a bit, or if they provide useful contrast. It's just easier to see the whole thing when it's all listed out in a sheet, rather than distributed across 400 pages.

[![A Desmos graph showing movable design variables](https://i.imgur.com/13KQsZo.png)](https://i.imgur.com/13KQsZo.png)

## Works best for generalized systems

[![A diagram of turns and action economy](https://i.imgur.com/CPPFhsK.png)](https://i.imgur.com/CPPFhsK.png)

On the downside, it does work best for generalized systems. By this I mean that AGD is more suited to something like SilCore or Oubliette Second Edition than say D&D 3.5e or Heroines of the First Age. Additionally, while it could help balance immensely, it would take a lot more time to use AGD tactics on a system where every class/job/archetype has its own fundamental mechanics.

## Leads to generative game design

Somehow, AGD always makes me think big. It believe it's because I've got tons of lego to play with, and part of me just wants to see what happens when I put those pieces together. Another part is that merely by having all those pieces to play with, you start to see opportunities you wouldn't have if you'd designed the system the "naive" way, by simply assigning rules and numbers to ideas as they came to you.

That generative angle can be *huge* too. Several hundred creatures? Possibly time consuming, but not a big deal. Dozens of weapons? Easily balanced. Etc.

## Document generation

We all know how much [I love document generation](https://write.as/arcandio/hi) and #automation. AGD feeds into that something fierce. Because you can program your sheet to spit out #Markdown, you've got instant access to updated stat blocks, ability descriptions, and so on, if you take the time to set it up. You could even pull data directly from a google sheet to be inserted into a manuscript on build, which would be absolutely fantastic.

## You can get trapped

One of the dangers of the spreadsheet-first method is that you can get trapped in what you know how to program. If you're unaware of something like `vlookup`, you might never think to make charts that you can reference elsewhere, which would seriously handicap your design ability. It's important to think about what you want to accomplish before you think about whether or not you know how to do it, then figure it out later. Chances are someone else has already figured it out. Spreadsheets are pretty powerful after all.

## It's hard to pivot

Because you build your core mechanics directly into the structure of the sheet, it can be difficult or time-consuming to go back and revise those core mechanics after designing a lot of asset instances, but there are ways to work around this if you're insanely clever. Other times, you just have to bite the bullet and accept that you need to change something with a lot of dependencies, because it will make the game better. In these cases, it can be handy to save off a backup, which doesn't work quite as good with a word doc as a spreadsheet, because again you'd have to change things in so many places in the purely text version.

## It may not be the right fit for the project

Finally, it's fine if AGD just isn't the right fit for a given project. Some narrative-oriented games just don't have the complexity to justify the spreadsheets, breakdowns, and analyses. Others are just too flexible to make mathematical sense out of in any semblance of reasonable time. Let me say this though: if your game is "too complicated" for a spreadsheet, it might be "too complicated" humans too.
