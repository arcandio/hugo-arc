---
title: "Oubliette Writing Portfolio"
date: 2024-04-29T12:07:18-05:00
tags: [self, writing, portfolio]
draft: false
featured_image: "images/oubliette-logo-red.png"
hidehero: false
---

This page has a few little snippets of writing from Oubliette Second Edition. They're not necessarily in order, and they span the entire 400 page book, so take each section separately.

> If you're looking to hire my services for writing, please contact me and I'll gladly send you any of my games to read as portfolio material.

---

# Book Introduction
## Awakening

You were expecting a burst of light. Your life should have 
flashed before your eyes.

But it didn’t.

Just blackness, everywhere, forever, and the sensation 
of falling. There’s not a sound, no rush of air on your skin, 
not even a proper locus of consciousness. At first, fear takes 
hold, what if this is all there is after death? But it makes little 
difference, there is no sleep, no rest, no stimulus to grow 
weary from.

Just as your mind begins to unknit, you wonder why 
you should even be if there is nothing to experience. Then 
it looms before you, a massive door, the size of the moon, 
writhing with fractal detail. It opens, drawing you in, and 
for the briefest of moments, you sense the millions of other 
spirits rushing through the gap headlong, then back to 
nothingness.

The sensation of falling increases, despite the fact that 
you have no senses, let alone a body with which to perceive 
changes in balance or acceleration.

It mocks you, this motion without self.

You become aware of a sensation, one that could only 
come from a having skin, a body with which to feel.
Cold.

It takes you a while to remember what it means. Cold 
and wet.

Now sound returns, bringing with it the drum and tap 
of rain. The splashing of feet. You try to move, but you are 
coming back only in parts and pieces, as though someone 
is reassembling your riven spirit like the pieces of a jigsaw 
puzzle. The pieces come together slowly. You can move your 
eyes, but not open them. You can feel the rain on your face 
clearly now, but you can’t move your head to shield your 
face.

Then things fall into place. Control over your body 
returns, and you gasp, desperate for breath. The sound of it 
echoes as you try to peel your eyes open.

## Where Am I?

It’s an alley. Stone. Slick with muck up to the hip, and 
you’re laying in it, slipping and falling as you try and fail 
to stumble upright. Irrational—or perhaps delayed—terror 
courses momentarily through your veins, causing you to 
cower into the shadow of a mound of decaying garbage and 
rot, from which you recoil moments later. Every new sight is a surprise, a shock.

As you try to calm yourself, a woman appears in the rain, 
at the end of the alley. She is wiry, dark of eye and hair, and 
wears a motley of rags that are soaked through to her bones. 
She raises her hands, as though in surrender, or in an effort 
to show that she’s unarmed. You mumble at her, but the 
words come out wrong; parts are still missing.

“Raste tranquilli, amui,” she says, carefully entering the 
alley. She seems to be leaving it open so you can pass.

“What…”

“Ah, I see. Be calm, friend. You are in no danger.”

“What… what happened?”

“You died, I’m afraid. My condolences.”

“…Is this Hell?”

“Only if you make it so. Relax, I am a friend.”

“Where is this? What’s going on?” you ask, your mind 
whirling and your heart in your throat. It should already 
be too late to panic, but your brain is unconvinced by this 
reasoning.

“You’ve died. You’re dead. Gone from the World of Life. 
This is someplace else. This is Oubliette.”

“Oubliette?” you echo, trying to latch onto the 
conversation to calm your unsteady nerves. She doesn’t 
seem so bad.

“Yes, Oubliette. Do you know what an oubliette is?” she 
asks, stepping into the alley further and out of the pounding 
rain of the street beyond. You try to ignore the hideous 
things stalking past in the rain.

“N-no… wait, it’s a dungeon, isn’t it?”

“Very good. Yes, this is Castle Oubliette, the dungeon at 
the end of the world. Welcome.”

## How Did I Get Here?
Approaching cautiously, she leads you further into the 
alley. She stops under an overhang of rotting wood, fabric, 
and leather from which dozens of glass jars hang, catching 
the rain. Faces peer over the edge and down 
at you from above, but they are shaped and 
colored wrong so you look away.

The woman wrings out her hood, sucking the 
rain from it and sighing, quenched. She waves to 
the green and brown creatures above, smiling 
at them. At your prompting, her attention 
returns to you.

“I suppose I should try to explain 
a little more, eh? Have a seat, you 
might faint,” she says, patting the 
muck next to her as she slides to 
the stones.

“This is Oubliette, where all things go when they are 
forgotten. Yes, even you. Now, brace yourself. A lot of this 
isn’t easy to hear.”

She adjusts herself, leaning toward you, to put her hand 
on your knee, as though to comfort you.

“You’re dead. Best to accept it now. This is the afterlife. 
And yes, it’s eternal. You’re stuck here. You’re just as eternal 
as everyone—and everything—else around here. You’re not 
going to Heaven or Hell, you’re probably not even going 
to leave this part of the Castle for quite a while. It usually 
takes some time to get used to the place. If you’re from 
Earth, you’re likely to see a lot of things here that you’ve 
never heard of or never believed could be real. And for 
good reason, all the different creatures and magics—well, 
nevermind all that. You’ll get the idea.”

The woman’s eyes flash and her hand lashes out like a 
lightning strike, then returns with an arthropod the size of 
both fists put together. Her eyes are alight with glee.

“Ooh, aren’t you a big one. Oh, apologies. You’ll get 
hungry soon, and finding food will probably be the first 
thing you need to do. This is an edipede. Good steamed or 
stewed, but you can eat them raw if you need. Want to try 
it?”

She smiles. “I thought not. But you will soon enough. 
Anyway, you’re what we call a ‘newcomer,’ which means 
you’ve only just arrived. And you’ll be here quite a while. 
You’re immortal, now, after all. I know, I know. Hard to 
believe. But you’ll believe just fine after you wake up from 
your first death.”


## Aren’t I Immortal?
“My apologies, that was cruel of me. Few people believe 
when they first arrive. But it’s important for you to 
understand that, deep in your heart, because if you don’t 
you might end up Broken.”

She points down the alley, where a pile of bodies lies 
in the rain. You stifle a flinch when you realize they’re not 
dead, just unable to move. “They’re Broken. Their minds are 
gone, shattered, and they’re likely to sit there for eons before 
that changes. So keep your wits about you, you’ll need them. 
Just because you come back from dying doesn’t make it 
pleasant. Those poor souls probably suffered one too many 
deaths, and now their minds are wrecked. Don’t let yourself 
become like them. Folk like you and me, still up walking 
around with our minds intact, we’re called Unbroken. Best 
to stay this way, if you ask me.”

She cracks open the stubby edipede and scoops out the 
colorful insides, shoveling them into her mouth. Up close, 
you notice that she has an assortment of charms and wands 
hanging from her belt, none of which are familiar. They 
look very old.

“Oubliette isn’t a kind place. Unless you’re quite strong, 
you may want to lay low for a while, so you can get the lay 
of the land. A strange place it is, and you’re going to see a 
lot of things you never thought possible. And, well, chances 
are, you’re here because you’ve got a little spark of the 
unusual in you too. If you nurture that, you may just make 
it here.”

The woman finishes her edipede and looks out at the 
dwindling rain. After a moment she carefully unwraps 
the age-spotted bundle she’d been carrying on her back to 
reveal a large tome.

“Here. Take this. It’s… a guide of sorts. Someday, when 
you’ve no need for it any longer, you’ll meet a newcomer in 
an alley, they’ll be lost and terrified, but hopefully the book 
will help.”

She stands and flaps her uselessly sodden clothes, 
stepping lightly back out into to the rain as you open the 
strange book. It’s filled with cramped text, illustrations, 
pressed leaves, specimens, and charts. There are even, on 
the first few pages, these very words.

Your eyes widen as the book tells you what the woman is 
going to say next.

“My name is Lucette de Ardes. I’ll be seeing you around, 
newcomer.”

# Caste Level Description
## Eldritch
*Rank 7, Godlike*
> The harbinger of doom came to our court. Ours. Of all the 
councils in Spearfield, Vlad visits ours the most. Terror 
gripped us, as you can surely imagine, and while the most 
brave of us spoke to this holy abomination, the strongest of 
our knights crept around to bracket her from all sides.
>
> I’m sure that Yvienne meant no harm, but it was her 
words that set the thing off. In an instant it had torn 
through the court, through the lords, through the 
knights, and through the walls.
>
> We were forced to abandon Toullers after of 
that. I’m sure they won’t forget. But I myself 
took away something different. Ambition.
>
> —Marie Gyul, supplicating to Linthara.


Eldritch is the true start of the highest 
Castes. These beings command powers 
so great that they can easily wipe aside 
armies of lesser opponents. They are 
hideously strong and most of them 
are highly intelligent and capable of 
extremely large-scale planning. Each 
has its own legend and history, and 
most have more than a few powerful 
enemies. For most denizens of the 
Interior, the eldritch Caste appears 
to be the end of the line.

**Primary Concerns**: The eldritch 
often rule the lower Castes, and thus 
have a number of responsibilities to 
attend to. Some lead organizations, 
while others make their way through 
the complex pantheon of Celeste 
politics unencumbered. Whereas many 
people of lower Caste ponder questions 
about what Castle Oubliette actually 
is, and how the cosmos is structured, the 
eldritch are the least powerful to begin to 
understand the answers to these questions.


# Location Description

## Cistern
*Rabble Ward*

Unfathomably deep and bone-chillingly cold, the 
Cistern is a small lake centrally located in the middle of the 
Fellmoor basin. No living person has been able to find its 
bottom, but there seems little point to that quest when the 
freezing waters of the lake teem with life. Crabs and eels 
and massive, dripping spiders the size of grown men are 
dredged regularly from the sharp dropoff just beyond its 
shores. The spiders are particularly delicious.

In addition to a thriving sprawl of fisheries, Cistern has 
several lakeside villas that—on good days, at least—actually 
resemble something like the Old World. These resorts offer 
fine dining, carefully escorted swims, a limited warranty 
against dying while on vacation, and all the creature 
comforts that a citizen of Oubliette could reasonably 
hope to expect. Of course, these services all come at a 
considerable premium, and it is not unheard of for some 
people to labor for decades just to afford a week on the edge 
of the Cistern. Apart from the villas and the fisheries, there 
are also many dozens of miles on unoccupied shoreline, 
where nothing but amphibious predators and the occasional hermit live.

Both the Draculeans and the Guild maintain villas 
here. Strangely, so too do the Purehearts. Sweetwater, 
their foremost spiritual training facility, is just a few miles 
down the shore from Aigues-Mortes: Vlad’s private estate. 
Rumors sometimes circulate about parties hosted by one 
villa for the other. Draculean courtiers especially love these 
stories, as they reaffirm their private belief that everyone 
would rule like Vlad, if only they could.

Most of the interactions between the Cistern and the 
rest of the Interior are driven by its fishing operations. 
Wagon trains from Grandhall and floating delegations from 
Celeste routinely stop by to fill their holds full of barrels 
of preserved crabs and spider legs in brine. Goblins from 
Mubigild come trailing battered sacks of salt, charging 
a high premium for the thick crystals that fill each bag. 
Occasionally, a merchant guild or Cutting gang will try to 
catapult itself into the big-time by taking the ward. During 
these brief but bloody conflicts, the fishermen put aside 
their grievances and summon towering elementals from the 
lake, scattering the invading warbands into the neighboring 
swamps of Fellmoor.

While Cistern has many huge fish to catch, it also has 
progressively larger monsters the deeper one delves into 
its dark maw. There are reports on the water’s edge of 
monstrous things thousands of feet down that actually span 
the entire width of the Cistern like trap spiders. Beneath 
them, the Cistern Spider waits. Folded up tight in the cold 
and quiet, its body is as big as a cave system. Its unblinking 
eyes watch the surface, as if awaiting some kind of signal. 
Legends still persist of the last time it came out of the lake. 
Back then, Mubog was still a district.


# Deep Lore
## The Veiled King
Shrouded literally and figuratively, the Veiled King is 
the closest thing Oubliette has to a god. Folk speak the 
name with great superstition. Many do not even believe 
that the Veiled King exists at all; that he is a confabulation, 
a bogeyman, a story told to put something in place of the 
divine.

But the Veiled King is as real as anyone else in Oubliette, 
perhaps more so. She is like a ghost that haunts the Castle, 
umbral and insubstantial. Her powers rival God’s and she 
is the sole purveyor of the portfolio of the Forgotten. She 
directs the evolution of the Castle, collects the Forgotten as 
they cross The Gate of Reason, and stands against the 
avatars of the Faraway Castles.

Nakaryon is her name now, from the Enteuil word 
“Anacaron”, possibly stemming from the Greek Acheron. She 
is not really a god, per-se, in that her blood is not divine, but 
her power is unmatched, particularly within her domain. 
She has lived many lives. She was once a woman of France, 
once a witch, then a queen, then a regent of a king, then 
the king herself. Oubliette is her castle, and it came to the 
World of the Forgotten through her own folly. On the Plain 
of Clouds she rebuilt it, protected it, and allowed some to 
reside there. She negotiated with unimaginable powers for 
a slice of infinity, and in that space she has done what she 
could in the defense of her people.

Because she was “written-out” of existence so 
vehemently, Nakaryon strongly identifies with the forgotten, 
exiled, shunned, and disenfranchised. After arriving in 
the Plain of Clouds, she began to gather such people to 
herself to protect them from the evils that lurk in the wastes. 
She rebuilt her castle as a way to defend them. Using her 
extraordinary magics, she explored the cosmos, and in so 
doing she realized that she could collect the outcasts of the 
World of Life directly from the Gate of Reason. She directed 
them to Oubliette, where—initially—she intended to make 
a utopia for them. Unfortunately, collecting such a valuable 
(if unguarded) resource from the World of Life made her 
enemies in nearly every other Castle on the Plain of Clouds. 
She fought them for a time, with True Vlad and a host of 
other ancient creatures known as The Army Hunters 
at her side, but retreated before the fighting became too 
intense, sensing that the war would be far more trouble 
than she could afford at the time.

So she returned to Oubliette and, against her own 
conscience, began rebuilding Oubliette. She took away 
her laws, stopped making appearances, and generally 
allowed the denizens to do as they would. She encouraged 
fighting, carefully steering her flock into a period of war 
and violence that she sincerely hoped would one day make 
them able to stand against the forces that have arrayed 
themselves against Oubliette. Ever since, her activities, both 
in the Castle and beyond, have been directed towards the 
winning of the oncoming cataclysmic battle, The Meta 
Warp. To that end, Nakaryon has retreated from the 
public eye, leaving much of the direct interference to the 
members of her elite intelligence and espionage apparatus, 
the Cryptarchy. Through intermediaries in Celeste and Slair, 
they communicate with her and receive complex, abstract 
orders, many of which lead to more confusion and violence.

For twenty-six thousand years, she has stayed this course, 
sticking firm to her philosophy, clinging to the ideal that 
in the face of an eternity of enslavement, the only rational 
choice is to undergo a kind of trial by fire so that those 
who survive have a chance at fighting back. But recently, 
within the past couple of millennia, events in Oubliette 
have reached such a fever pitch that she has begun to 
question whether her decisions have really been the right 
ones. Nevertheless, she will not permit anyone to speak 
against her policies to her face, unless they have earned that 
privilege by overcoming her test. That precious handful of 
confidantes she has know that she secretly weeps for what 
she has done to her people, and that she fears that there isn’t 
enough eternity to for them to learn to forgive her.

