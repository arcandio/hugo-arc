---
title: "More Wip Wednesday, and Home Improvement Projects"
date: 2020-06-24T10:20:11-05:00
tags: [art, wip, woodworking, home-improvement]
draft: false
featured_image: "/images/2020-06-24-10_01_21-Window-hero.png"
hidehero: true
---

I *feel* like I haven't been keeping up with my blog lately, but then again, I've also just been getting a lot of stuff done.

# Art Stuff

![shadows and lighting on a thicctailed critter](/images/2020-06-24-10_01_21-Window.png)

Finished a LOT of stuff on this, an entire shadow pass and lighting pass in about an hour. Phew. Hand's sore.

![Raffinas and Meepo chilling in color flats and shadows](/images/2020-06-24-10_02_59-Window.png)

Started the color flats and got most of the way through the shadow pass as well. Still needs some work on the lighting, background work, some gradients, and possibly some depth of field blur. We'll have to see how that looks.

# Home Improvement & Woodshop

Man, we've been getting insane amounts of stuff done around the house lately, and my shop of woodworking tools has *massively* expanded lately. Should have put it all on the credit cards, for points or something.

New tools:

* **Hand Plane**: worked pretty well, considering the torture of wet, knotty, crappy wood I put it through.
* **Plane iron & chisel sharpener**: works good, now to try the plane on something bigger and smoother
* **Pegboard**: freaking awesome for organization, should have done this years ago
* **Air Compressor & Finish Nailer**: works pretty good. Compressor is loud, but I can put it outside the garage. I plan to use it for helping my folks put up new trim.
* **Kreg Jig**: has proven *very* handy for some difficult to attach stuff.
* **Bigass Shopvac**: about a day later, the old home depot buckethead died on the spot, probably from shock.

I've been really impressed with how cheap I've been able to get this stuff. A lot of bargain hunting, though I still can't figure out how to find used tools around here.

Tools on my radar:

* Router / router table
* Drill press
* Band Saw

## Misters

<video controls loop autoplay inline muted>
    <source src="/images/20200619_172315.mp4" type="video/mp4">
    Sorry, your browser doesn't support embedded videos.
</video>

It's really hard to take pictures of this, but I installed a set of water misters over the deck, and hooked it into our new backyard hose setup (which still needs a hose manager thing), and while it's been raining lately, we found out last weekend that the misters are pretty dang nice on hot days, and the project was cheap AF to build. It's also surprisingly high up and easy to adjust; it uses paracord around the tree and some adjustable friction hitche knots to raise and lower it.

## Organization

![tools on pegboard](/images/20200618_134014.jpg)

> And this isn't even my final form!

Since this photo I've gotten a lot more stuff on the wall, including all my remaining drill bits, more screwdrivers, knives, my mini block plane, and some other odds and ends.

I'm hitting my stride with tools now. I've got a 3d printer with which to print custom tool holders for the pegboard, and I have the equipment to build a nice replacement for my miniatures & terrain workbench in the basement. Getting to the point I can do a *lot* of things myself around here.

![upright wood storage](/images/20200612_072326.jpg)

Looks like a lot of wood leftover from the deck, and it is, but a good portion of that was used to make the benches in the next section, but there's still a goodly amount left over for shelves in the garden shed. Most of that 8 foot stuff is upright, while there are secretly still some 12 footers hanging out under the bottom that you can just barely see in the corner of the photo.

Too bad it's all AC2. I need to get some cheap 1x pine boards and 2x4s to do *interior* projects with. 

## Aldo-style Benches

For our nice new deck, I decied to make some benches with the spare wood. The first thing that came up on Google for plans was a reimagined [Aldo Leopold style thing](https://rogueengineer.com/diy-outdoor-bench-plans-with-back/) by Rogue Engineer.

Unfortunately for me, his is built using 2x8 lumber, which I didn't have, so I redesigned it to work with 5/4 deck boards, which I have a lot of.

![Aldo-Leopold-style bench 1](/images/20200620_085844.jpg)

This first one was actually *too* similar to Rogue Engineer's, his has a butt joint for the backrest, and his was end-screwed, a bad woodworking joint. So to remedy that, I used my sweet new Kreg jig to put much stronger pocket hole screws in.

![The other bench out front on the porch](/images/20200620_115156.jpg)

It's hard to see, but I did something different with the second one. I shortened the seat by a couple of inches and did a lap joint for the backrest, making it both easier to assemble and probably stronger.

For the backs of these, I made one of my first table saw jigs, a taper jig using those drinking-bird-shaped clamps, and a piece of shelving. Worked surprisingly well.

## Deck

New deck is working great, so here's a photo of it in its natural habitat.

![our new deck](/images/20200620_123535.jpg)

Now, if only I could fix the dishwasher and oven with a tablesaw and an impact driver.