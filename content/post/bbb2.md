---
title: "BBB2"
date: 2015-03-09T15:10:16-05:00
tags: [programming, c-sharp, unity, portfolio]
draft: true
featured_image: "/images/bbb2-wide.png"
hidehero: false
tech: "Unity, C#"
description: "Desktop app for designing flower bouquets in 3d"
year: 2015
---

# Description

## Business Purpose

## Structure

# Process

# Challenges

# Points of Interest

# Critiques

# What I learned

# Demo

