---
title: "A Busy Weekend Rescuing Fish"
date: 2020-07-20T10:09:12-05:00
tags: [fish, woodworking, wip, art, blog, home-improvement]
draft: false
featured_image: "/images/20200718_163956.jpg"
hidehero: true
---

What a week. Jeez.

Last week I had a fishy catastrophe. My 40 gallon freshwater tank developed a leak at the very bottom seal, and basically dumped its guts on the carpet in the basement over the course of about an hour. I got most of the fish out in time, but there were a couple of casualties, including our favorite, a huge but shy female rainbow shark. We buried her in the backyard. The survivors got to stay in a 10g emergency hospital tank, which probably wasn't cycled up properly.

Because of that, I spent 2 entire days last week speed building a new fish tank stand. I'd already planned to do so, because the old one was a cheap, particle-board store-bought POS, but I didn't plan to be working on it in such an emergency.

![blender 3d](/images/20200715_134959.jpg)

I had to do a lot of measuring, checking, and redesigning very quickly. It's also a good thing that I can fit 8 foot boards in my podunk Sentra.

This deal also came in *very* handy, and it's a good thing I built it last weekend, or this week would have been hell.

![miter saw stand](/images/20200714_173956.jpg)

In the back, there, on the workbench, you can see the first couple of pieces of the tank stand. The long fence on the saw made it so I could mark and clamp stop-blocks to it, but since it's made out of junk deck boards, it's not very square and eats a lot of space. I should replace it when I can.

![framing happened](/images/20200715_102652.jpg)

Woosh! The first part of assembly (the interior top frame) went by pretty fast. I'd cut everything[^1] to length, so it was pretty much just a shit-ton of pocket hole drilling. For the flat parts, I just used deck screws with no pilot holes. Nothing split, so that's good.

![suddenly table](/images/20200715_131924.jpg)

Clamping that shit was kind of a pain though, but joints are just hard to stabilize. I need more clamps.

The top isn't glued, it's just clamped and air-nailed down. I ended up needing to make an entirely separate top piece so that the stand could fit into the space (more on that in a bit) but the tank ends wouldn't be unsupported.

You can see some of the fasteners from this angle because the whole thing is going in a kind of cubby-hole closet thing.

![woosh stain](/images/20200715_152446.jpg)

Once assembled, I basted the thing in black stain. Mostly. The back is going to be unseen unless you feel like moving a 500lb glass fish tank, so who cares. I sprayed the whole deal with polyurethane twice, but it didn't take the kind of shine I had hoped for. Oh well, gotta get this done so that the fish don't die in the 10gal.

![in situ fish tank and stand](/images/20200717_165228.jpg)

Sorry for the glare, that's the living room lamp right there on the other side of the wall. This shows you the space I'm working with. The tank is 48" long, but the space between the mouldings is 47 1/2". So the stand is 47" and fits right in there (and its heavy as FUCK) and the topper is 48 1/2", so the whole thing fits into the closet thing.

You're looking at it from the living room. To your left is the kitchen, and directly past the tank is the library[^2]. To the right is the rest of the living room and the door out to the [BRAND NEW DECK](/post/rebuilding-the-deck/). If you back up from this position, you'd trip and fall into the wife's chair, and mine is next to that.

![a bit more context](/images/20200717_174715.jpg)

Those are the guest/cat chairs with blankets on them. At this point, I've filled the tank and added the substrate (switched out Eco Complete for normal old white sand, cause we have Cory Cats) and placed in my plants and wood. Running the filter on full blast to get the water, nitrobacter and nitrosomonas moving around. You can sort of see a white lump in the tank, that's the filter media bag from the previous tank, which was saved because I had a big sump. With that, I sort of don't have to cycle the tank in the normal way, because I've just transplanted the filter media. I'll need to wait for the bacteria to colonize the new tank before I can remove it, but at least I can get the fish in there faster. Not yet though, until I've done several water checks for ammonia, nitrites, and nitrates.

Some time later:

![fresh, planted, empty tank](/images/20200718_163956.jpg)

Nobody wanted the cucumber yet. Such is life.

![looks a ghosttown](/images/20200720_062439.jpg)

Well, here we are. Pretty empty right now: about 1/4 to 1/3 stocked, depending on how many plants and how much sump I eventually add. We're actually looking at moving some of our fish down to the 10g, just so they stop bothering the slower, more chill fish.

Since you made it this far, here's a plecoface.

<video controls loop autoplay inline muted>
    <source src="/images/20200718_153511.mp4" type="video/mp4">
    Sorry, your browser doesn't support embedded videos.
</video>

# The Anti Strahd Squad

Not a lot of [#WIP](/tags/wip) stuff lately, because I've only had one commission for a while, but here it is, in all its glory.

![The Anti Strahd Squad](/images/ASS.jpg)

# Some D&D Was Also Played

![a random dungeon](/images/20200707_215522.jpg)

I've been running some small D&D sessions for my wife. We've been trying to sort out our feelings about D&D 5e and the community and culture lately, so we haven't been playing as much D&D the past few months. I'm going to save that stuff for a different blog post, but we've found that a certain level of distance from the game actually allows us to play more. When we treat it more like a board game, it feels less constricting to storytelling. So we've been running random dungeons for each other.

This one, I designed as a series of 6 trap / puzzle rooms, each with a button that was duplicated in the hub. The buttons in the hub have to be pressed in the right order (they correlate to 6 angels, whose names are in alphabetical order) but they have to be enabled by figuring out each room first. The entire thing was supposed to be intricate and allow for good environmental storytelling, but I found that since we were kind of messing around, I wasn't that invested in playing out the encounters as tense or exciting, a lot of dumb monsters saying silly things and doing their own stupid shit happened. Wife said she really enjoyed it though, so that's good. Didn't go the way I wanted, but that's the sign of DM hubris.

Now I can focus more on our Oubliette Remote game, our personal new pet project, and possibly thinking about a new horror campaign.

# And other things

- Installed a plant hook on the shed
- Installed brand new under-cabinet lightinging in the kitchen, it's gorgeous and tied into the normal light switch over the oven
- Got a new gas range installed
- Slow-cooked some pork with two different spice rubs to see which wins

Here's an cat, outside.

![outside cat](/images/20200716_163007.jpg)

man I'm tired. How many days until the weekend?

[^1]: Except for that whole, "I want it to actually be 10 inches taller, because you can't see it from my chair, and now I have to cut new legs to fit" thing
[^2]: that someone mislabled as a "dining room"