---
title: "Purple Character Commission, Finished"
date: 2020-07-01T09:41:30-05:00
tags: [art, blog]
draft: false
featured_image: "/images/May-purple-hero.jpg"
hidehero: true
---

![all finished an shiny](/images/May-purple.jpg)

I don't actually have a lot to say about this one this time. Looks pretty good overall, I think. I feel a little bad about how the lighting mostly overrode the two-tone skin coloring, but aside from that, I think it came out pretty well, and it wasn't a terrific pain int the ass like a lot of other stuff has been lately, on the programming side. Good client, good reference, good flow state.

I particularly like the overall feel of the lighting, I feel like I was able to draw a lot of focus this time because of the clothing colors and focal elements.