---
title: "Hobby Collecting"
date: 2024-04-29T10:07:18-05:00
tags: [blog, self, hobbies, meta]
draft: false
featured_image: "images/gluttony_wip.jpg"
hidehero: false
---

Hello, blog. Long time no see.[^1]

> I meant to make my next post about our cat Ginger[^2] who passed away recently at the age of 20[^3], but we haven't been able to do that yet. Wanted to put up some photos and stories.

ANYWAY, today, we're going to do a list of all my hobbies, and their current status. It should be pretty entertaining. I jumped back in on a couple of things lately, and it's caused some people to make note of my ability to be a Renaissance man.

I'll be coming back to this list to update it from time to time. Or maybe I'll just duplicate it so I have a record of my adventures.

|Hobby|Frequency / Recency|Status|
|-|-|-|
|3d printing|weekly|worked on freebies for cons, still selling stuff on etsy. Printed some cool terrain. Learned I should print resin at 10 microns.|
|Astronomy|infrequent|got to do some great observing of the solar eclipse and taught a ton of astrononmy facts to folks in the field.|
|Board Games|bimonthly|Stellar was good, Ecologies is great, could go back to Civ sometime|
|Camping|upcomming|excited. New fast setup tent, new fast setup screen tent, new beds, whee.|
|Cosplay|infrequent|Dark Dragon costume looks cool, lots of bits. Star Wars sith costume is OK, but low saber practice|
|Dice Making|weekly|going well, several sets to paint pips on. Need to do a blog post to show off.|
|Guitar|weekly|mostly just tooling around, no real progress|
|Hunting|seasonal|bagged a deer this year, broke it down, processed it, and butchered it ourselves. Warm enough to go to the range for practice again now.|
|Kayaking|whenever it's 70+|FIRST KAYAK OF THE SEASON DONE YEAHHHH|
|Leatherworking|recentish|trying to get back on the horse, spin up some leather carving, but lots of other stuff to do. Todo: Sporran, multitool pouch, leather armor|
|Lightsaber|stalled|haven't practiced choreography since last year, sad. Hard to practice in the winter.|
|Mini Painting|daily|lots to do, learnging and practicing regularly, streaming is pretty good|
|Music Production|infrequent|no recent projects, but many ideas|
|Photography|weekly|does this even count? I take photos wherever we go. We did do a real photo trip to the arboretum the other week though.|
|Sewing|recent|sewed a bag, two cloaks, and dyed them, working on a hat and another cloak. Repaired and learned to use a serger we got 10 years ago.|
|Sketching|infrequent|hard to get back into the habit, need a schedule, have a new ipad to use as a sketchbook|
|Tabletop RPGs|weekly|Playing in 1, running 1, setting to run 1|
|Video Games|biweeklyish|Playing Nier Automata, Diablo 4 slowly, and Call of the Sea|
|Wargaming|few months ago|haven't played for fun in a few months, but still taking Eradicus to cons|
|Woodworking|monthly|just turned some toggles for my bag and costume on the lathe, worked great!|

[^1]: That seems to be the way it goes these days.
[^2]: Best Cat.
[^3]: that's like 96 in people years.