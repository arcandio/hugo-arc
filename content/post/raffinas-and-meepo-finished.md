---
title: "Raffinas and Meepo Finished"
date: 2020-07-01T09:41:11-05:00
tags: [art, blog]
draft: false
featured_image: "/images/linton-raffinas-and-meepo-hero.jpg"
hidehero: true
---

![raffinas and meepo, completed](/images/linton-raffinas-and-meepo.jpg)

I really need to figure out a good workflow for publishing my artwork.[^1] I'm getting better at posting art here to my blog, but that seems to have come at the expense of keeping up with my instagram.

This piece seemed to go pretty well too. It took some time to do the lineart, but with 2 characters it came out to about 8 hours, which is about what I'd expect any other way I'd go about it. I'm not *ultra* happy with Raffinas's jawline, and I had hoped to have some time to do [knockouts](/wiki/knockout), but it is what it is. You have to call it done at some point.


[^1]: I don't mean generally, I get plenty of it into production in my RPG books and in other people's RPG products. I mean the daily-basis, commissions-in-commissions-out type stuff, that I post here.