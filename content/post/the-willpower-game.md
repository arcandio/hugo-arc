---
title: "The Willpower Game"
date: 2019-02-13T10:33:15-05:00
tags: [blog, self]
draft: false
featured_image: ""
hidehero: true
---

> “The primary sign of a well-ordered mind is a man’s ability to remain in one place and linger in his own company.”
> —Macy Halford

Work is hard. I don't know if your job is like mine, but if it is, there are times when you just don't have the "energy" or "ability" to continue your tasks without some kind of break or renewing fuel or something.

For me, my job is mostly like this:

![flowchart of my daily work](https://i.imgur.com/ZWPi3xc.png)

But the diagram doesn't really cover why sometimes I feel too lazy or not energetic enough or too daunted by a task to complete or even approach it.

> Aside: I'm a problem solver. It's what I was trained to do, it's what I've learned the hard way to do, and it's how I get anything ever done. So this isn't really about a lack of problem solving ability, it's about motivation.

So that's where #TheWillpowerGame comes in. As a Game Designer, this fits nicely into my head: there's some kind of game, **a system of rules with a goal** that my daily work operates on (for the most part at least), and if I can figure out what the rules are, then maybe I can play the game better.

Here's the catch: *I don't know what the rules are* and furthermore, *I barely even know the moves.*

But I can at least start documenting my attempt at understanding this game.

## The Basics

I seem to have a finite amount of energy to do tasks throughout the day. For the sake of argument, let's call that "**willpower**."

In my case, and in the current setup of [Voidspiral](https://voidspiral.com/)'s project structure, I've got two pools or scopes of willpower: daily or short-term willpower, and project-wide willpower.

Daily willpower ebbs and flows based on the tasks I have to do that day and what stands in the way of their completion. Some tasks I *want* to do, while others I'd very much rather not.

Project willpower ebbs and flows based on how connected to the project I feel and how exciting it is for me. It's also more abstract and hard to describe, define, and analyze.

So at a high level, there are things that I can do that *give* me willpower, and there are things that I must do that *reduce* my willpower. Those things may either take meaningful time to complete, take a trivial amount of time, or in rare instances, actually provide a net income of time back into my day.

## A Complex Ecosystem

So I spent some time to try to analyze the types of tasks I do, and what their effects are.

[![A massively complex and interconnected flowchart](https://i.imgur.com/eTtiaG0.jpg)](https://i.imgur.com/eTtiaG0.jpg)
<sub>warning: this is in no way complete.</sub>

You can click the chart to open it larger; it is pretty rambling. Sorry that it's not more clear, but the elements are interrelated in very complex ways that make it multi-dimensional and hard to flatten out clearly.

**How it works:** Each edge is directed. The arrow points from the thing providing willpower or time to the thing consuming that resource. Some things consume multiple resources, like time and willpower, while other things benefit other tasks rather than resources directly, for example how #automation benefits efficiency but not willpower.

You can see a lot of interesting chains of influence here. I particularly like how `jamming the hell out` boosts `flow state`, which in turn boosts `efficiency`, all of which boost short term willpower. This part of the model rings true for me: I really like listening to music at work, and it's very important to my day to have [good tunes](https://www.last.fm/user/arcandio) to listen to.

## Observations

What can I learn from this?

The green items (which I'll just call "fun things") isn't completely accurate, since some of them (`side projects`, `fun things`, `new projects` etc.) don't show a time cost. I'll call red items "hard things."

I can now identify things that are either integral or powerfully helpful by looking for highly-connected elements and chains. For example:

* `Easy tasks` build momentum, and both benefit short term and long term willpower.
* `jamming the hell out` ► `flow state` & `automation` ► `efficiency` seems to control a large portion of the game, both from the chart and in real life.
* `Exercise`, `entertainment`, and `relaxation` all feed into `stamina`, but there isn't much more I can add to that in the time that I have every day. Almost all my non-work time fits into either one of those three or the `chores` group.
* There's an unfortunate deficit of solutions to problems that actually feed back into my daily willpower pool. I probably should have colored those purple or something, because they don't really fit in as "fun things."
  * at least `automation` feeds back in. It does seem to help out a lot, anecdotally.
* There are *some* tasks that have no time-cost. `Treats/surgar`, `get blankets/fan` are both there, which is interesting because I rarely do those things when I should, judging by the diagram. I guess that fits them into the `interruption` problem, so it's obviously more complex than I thought.


## What Can I Learn Here?



* If nothing else, having this diagram around can make it easier for me to figure out how to come up with a little more willpower to finish something out.
* Thinking about my willpower levels also seems to have the side-effect of either giving me more willpower or reducing the willpower cost of hard things.
* This approach naively skips a lot of motivations and necessities for the tasks. For example, `social media` is my main marketing venue, so even though it doesn't have a green arrow producing value on the diagram, I have to do it anyway.
* Knowing what the hard things are will help me to either avoid them or set up solutions ahead of time, which should in theory help my willpower.
* The diagram doesn't take into account physical tiredness. After I `snowblow` or `mow` I'm physically tired in a way that my willpower can't make up for, *especially* if the tasks I need to do are mostly art-oriented.
  * Likewise, I can wear myself out drawing for a few hours, leading to *both* less willpower *and* physical tiredness.
* Another major factor not shown here is my creative anxiety. I don't even know where to begin on that.



## Where To Go From Here



I've been thinking a bit about taxonomies and ontology lately. Maybe, as complex as it would be, it'd be possible to further refine my definitions and start applying categories to these task types. I'm not sure if that would help me, or just be a fun way to waste some more time.