---
title: "Why Corel Painter Pisses Me Off"
date: 2019-03-26T10:36:09-05:00
tags: [software, rant, blog, art]
draft: false
featured_image: "/images/Emar-shargat.jpg"
hidehero: false
---

*Spoiler Alert: I like it, but I don't. It's complicated.*

* even 2018 is slow and stuttery on my machine, which runs Clip Studio Paint and Adobe Photoshop just fine
    * Even just doing simple UI things take a long time
    * Jitters and UI stutters drive me insane
* Various layer blending problems
    * white edges around some transparent layers
    * Why have "Default" and "normal" both? whyyyyyy
    * Other blend modes seem redundant as well, Shadow Map vs Multiply?
* Very little support community. More people seem to be talking about Clip Studio Paint than #CorelPainter, and it's a port of a Japanese product.
    * Searching for help topics for corel painter routinely includes other corel apps instead of painter
* no options for color depth like in Adobe Photoshop
* Not as familiar with the brush system (100% my fault)
* Various missing simple features
    * Can't duplicate or convert canvas to normal layer
    * can't move the canvas layer using the normal move tool
    * Tab closes the navigator as well (same with PS and CSP, but they have a work around, see next)
    * Can't duplicate the view of a single file to use it like the navigator
* Nag screen to buy the next product, when I already paid my money
* Pressure sensitivity lacks finesse
    * the pressure curve is global to the program, not local to an individual brush, like it is in Clip Studio Paint
* Less smoothing options than Clip Studio Paint, but more than Adobe Photoshop at least
* Differences in navigator view and canvas view interpolation make the textures look *very* different in each. No idea what the image actually looks like.
* magic wand selects regions along the image border when it should stop at the image border.
* poorly socialized. Does not play well with others, like Photoshop. CSP isn't spectacular at this, but it's at least plausible.

# Why it bothers me

I wouldn't normally just bash on a product I don't like just because I don't like it. But here's the thing, it's a love/hate relationship. I can get some *amazing* output from the tools in painter, but *at what cost?* Even my wife comes and tells me to stop using it when I get too frustrated at it. But you can directly compare the brush quality of this image made in Painter:

![A half-orc painted in Corel Painter](/images/Emar-shargat.jpg)

with this one, made in Clip, attempting to emulate the same sort of brushwork:

![An elf painted in Clip Studio Paint](/images/Wynis-Nyrn.jpg)

I've been working on trying to translate some of those features, but so far it's been a pretty tough battle, and I'm not really winning it.

I should try again though, I've got a lot of CSP brushes I want to make. Just need a list and some time.