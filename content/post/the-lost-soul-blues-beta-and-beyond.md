---
title: "The Lost Soul Blues Beta and Beyond"
date: 2019-03-13T07:22:51-05:00
tags: [game-design, blog, tabletop-rpgs]
draft: false
featured_image: ""
hidehero: true
---

*I just released The Lost Soul Blues, a short dark-fantasy #ttrpg set in Hell.*

[So I made a thing.](https://www.patreon.com/posts/beta-lost-soul-25324954)

It's the first time I've written such a small game, and it was a liberating experience that I *greatly* enjoyed. In fact, there've been times when I've had to tear myself away from it to do other things. Not just work on The Dawnline and commission art, but like, dinner, and the dishes, and hanging out with my friends.

I consider this a win. It's nice to have a project that reminds me that it can be a lot of fun to design games. I'm glad I worked on it, and I'm looking forward to seeing what people think, if anyone plays it.

## What I'd like to do

**Feedback:** It's a small game, but it's not done yet. It's not well tested in play yet, so I'd like to get some feedback from players and GMs who aren't me before I put the official stamp on it. Fortunately, it's a small game, so it shouldn't be hard to read and give feedback on.

**Lore Page:** I'd really like to do a fourth page for the game that briefly covers some of the lore "behind" the game. I say that in quotes because there *isn't* much lore back there yet, but I'd really like to add some, if only to help GMs who are unfamiliar with the themes.

**Fancy-shiny Premium Version:** I think it'd be really cool to do fully painted art and reverse the colors so that it looks more like a poster. That way I could sneak in some more texture that would help sell the feel of the game. I'd love to contract Wayne Douglas Barlowe (my freaking idol) to do the art, but I'll probably never get that to happen. Alternatively I'll do the art myself, of course, but the problem with that is how high I've set the bar. I've never tried to really work in Barlowe's style, so if I want to do that, I'd better practice a bit first. If you want to see that, let me know.

**Four-Page Fold Up:** It'd be cool to print the 4 pages on an 11x17" sheet, folded in half, to be released at conventions and in stores. This would probably be best as the fancyshiny, but I think it'd be really cool to have around.

**Lore Supplement:** I'm also thinking about writing some short stories, snippets, and maybe even comics set in the world. This would be a standalone thing, not required to play the game, but could help GMs figure out the themes and lore of the game better, plus it'd be cool. I'm not sure if that'd be a paid deal, if I'd offer it for free digitally, or if I'd print it or what. Opinion?

**DTRPG:** Once I've got some feedback, I'd like to put the game up on DriveThruRPG as well, possibly as a Pay What You Want.

## Why I liked it so much

I think this thing might just be the culmination of a huge amount of my creative life. I grew up on grimdark and Barlowe and Hell and Diablo and so on, and so #TLSB is like, *part* of me, ya know? It's just loaded with so much of my stylistic opinions and favorite lore bits. Each of the potential masters is a character from one of my past games or stories, which some of you may even remember.

What could be cooler and more badass than being a demon and fighting other demons? AUGH it's so cool.

## How free?

Well, it's currently under CC BY-NC-SA 4.0, and you might think that I'm only putting it up for free because it's a [VEGI](https://www.patreon.com/voidspiral) beta, but I plan on #TLSB being free even when it releases. I may offer paid versions, like the printed versions above, but the base game will be free. Obviously there are a lot of reasons for this, not least of which who would pay for a 3 page game, but also because I as many people to enjoy it as possible.

## Support

The Lost Soul Blues may be free, but it did take some work to make, and it'll take more work to make the rest of the stuff I want to do with it. If you like the game or the themes or even just the art or something, consider supporting me on [Patreon](https://www.patreon.com/voidspiral) or via [Paypal](https://www.paypal.me/voidspiral). You can also find my games on [DriveThruRPG](https://www.drivethrurpg.com/browse/pub/5015/Voidspiral-Entertainment).