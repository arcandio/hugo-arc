---
title: "Assaj Underpainting"
date: 2020-06-09T10:44:15-05:00
tags: [blog, sketch, wip, art]
draft: false
featured_image: "/images/assaj-underpainting-hero.png"
hidehero: true
---

![underpainting of a lizarfolk](/images/assaj-underpainting.png)

Back with the next phase of this lovely lady and her noodly pals. I'm not super stoked about the scarf highlights or the amethyst jewels, but at least it's going quickly now that I'm back to work.