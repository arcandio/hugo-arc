---
title: "JTP1: Space Paranoia"
date: 2021-01-26T11:27:18-05:00
tags: [blog, tabletop-rpgs, podcast, joes-table, worldbuilding]
draft: false
featured_image: "/images/jt-podcast-cover-with-bg.png"
hidehero: true
---

[![jtp logo](/images/jt-podcast-cover-with-bg.png)](https://anchor.fm/joes-table/episodes/Joes-Table-Podcast-1-Space-Paranoia-ep72qf)

<iframe src="https://anchor.fm/joes-table/embed/episodes/Joes-Table-Podcast-1-Space-Paranoia-ep72qf" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>

- https://anchor.fm/joes-table/episodes/Joes-Table-Podcast-1-Space-Paranoia-ep72qf
- https://open.spotify.com/show/11NMS8T1EppUemE8MQfX3J
