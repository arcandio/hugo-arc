+++
date = 2020-07-23T12:30:00Z
featured_image = "/images/antum-environment-sketches-1.jpg"
hidehero = true
tags = ["sketch", "art", "blog"]
title = "Antum Environments & Perspective Sketches"

+++
![frigid northern cannons](/images/antum-environment-sketches-3.jpg)
![an ancient monolith](/images/antum-environment-sketches-2.jpg)
![round rocks](/images/antum-environment-sketches-1.jpg)

Not really very happy with these pieces except the last one, but it's been a long time since I painted landscapes let alone in the vein of concept art.

![perspective sketches](/images/perspective-practice.jpg)

also finally trying to spend some time in the mornings just practicing. Some perspective sketches, a mix of using the Perspective Rulers in Clip Studio Paint and freehand with the line tool.