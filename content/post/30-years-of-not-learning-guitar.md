---
title: "30 Years of Not Learning Guitar"
date: 2021-01-20T11:07:18-05:00
tags: [blog, music, guitar, self]
draft: false
featured_image: "images/20210120_101827.jpg"
hidehero: false
---

Hello, blog. Long time no see.

I've picked up[^1] guitar once again. And this time it's for real.

I've loved music, specifically rock & metal, since I first got a hold of *Master of Puppets* sometime before 2003. I even started learning to play guitar when I was very small child, probably around 6-8. Over the many years, I've kept picking it up, then putting it down. I've had a small but growing collection of guitars for about a decade, and now the collection has expanded once again.

But more importantly, I'm finally starting to feel like I can make real progress. I think I just needed to remember how to *learn.* As a kid, I had approximately 1 music class where I borrowed (and ruined) my dad's old acoustic. In my twenties I got a cheap strat and had no way to learn how to play it aside from a single chord book. In my thirties, I got Rocksmith and an ESP Les Paul Special II, and that was probably the best I thought I could do. But I've recently come to realize that Rocksmith is actually a pretty bad way to learn guitar. It pretty much skips any kind of confidence building excercises, going directly into "play this lick" kind of content, which is extremely frustrating. And more importantly, the core mechanism of "play this song" doesn't really teach you a simplified version of the song first, it just has you play a couple notes from each song and moves on. I don't think you'd really know how to *play* the song until after you'd mastered it on rocksmith, which is kind of a backwards way of doing things.

# Trying Again

But this Christmas, I got myself a couple new guitars, and this time a new strategy. I got a resonator as a replacement for my dad's poor old Hondo, and I got a cheap bass guitar both. I'm not sure whether I feel like I'd rather play bass, lead, or rhythm, but now I've got all the options. And as usual, I'll probably be trying all of those things to some medeocre level.

Additionally, I've gotten on the [Justin Guitar](https://www.justinguitar.com) / guitar youtube train. Justin teaches excellent lessons aimed at actual beginners (which I guess I still am, even though I've been trying for 30 years), and walks you through the smallest increments of progress, with the perspective of someone who's *actually learning,* not just a master who already knows everything and doesn't know how to teach it to you.

# Practice Practice Practice

So between the resonator[^2] (awesome bluesy sound, so great) and Justin Guitar, I've been practicing for about 1/2 an hour every day for the past couple of weeks. That's a lot more practice and progress than I put into Rocksmith a few years ago. Obviously, I can't play anything really substantial yet, but I've got some blues jams and improv stuff I can do, and that's fun enough to keep me playing for longer than I commit to each time. I'm actually having *fun.*

Now, I'm not going to say that it isn't hard. It very much is. I've only just gotten enough calluses for my fingers not to be super sore after playing[^3], and I'm still working hard on chord changes and rhythm. I've got a hell of a long way to go.

# The Bass Deal Ever

Let's talk about the bass for a sec, because it's hilarious.

See, I had some money for Christmas, and decided that I really wanted to get back into music for real, but I wasn't sure whether I wanted to play bass or guitar. I'd never tried bass, and was very curious about the bass path on Rocksmith (at that exact moment, I'd not been introduced to Justin  Guitar). So I went looking for cheap bass guitars in the Madison area, and found my target, a cheap black squier bass guitar that had clearly been dropped on some sharp concrete or a meat grinder. It was a steal at $80, compared to other instruments I was looking at, so I bought it on pickup from my Madison store.

It turns out that the guitar wasn't *in* the Madison store. The shop I'd bought it from was a small chain and the guitar was somewhere *else.*[^4] So instead of being able to pick it up in the couple of days between Christmas and New Years, I waited.

And waited.

I emailed a few times, and called a few times, and each time they told me it hadn't been shipped from the other store yet.

So I asked "why?"

The answer, I found out several days later, was that they'd (of **course**) sold it to someone else, who'd walked out with it.

So instead, they offered me a different bass guitar. I accepted.

So instead of a dinged up old baseball bat for $80, I got an apparently brand spanking new Squier worth probs $230 for the price of $80 and frustration.

So, uh, thanks I guess?

I'm not naming the store, because I'm still not sure if I feel like I'd give them a 👍 or a 👎 because of the whole thing, but the store looked very nice when I picked up the bass, and they seemed frendly enough, so I think I'll probably remember this fondly later on.

# The Strategy

There's a reason I'm collecting all these instruments (and the keyboard I've dug up and the midi controller I'm eyeballing).

I know for a fact that I'm a flighty butterfly of hobbies. This is just objectively true.

But this time I think I can outwit myself by putting interesting thingies all around myself. If I get tired of guitar, I can try bass for a bit. If I tire of strings, I can try playing keys. If I get tired of instruments, I can work on music theory and production. And when I tire of that, I've got a guitar that needs some attention. And around we go.

To this I'm also supplementing a rich diet of apps. I've got a whole new folder of them ranging from metronomes to ear training to fretboard learning to rhythm training. I don't play these religiously, but they're sure a better way to waste time for a few minutes than reading more *news.*

---

So that's the deal. I'm trying to get back into guitar. I'll try to stick with it this time, and you can expect some more blog posts of me complaining like whiny little bitch about how hard it is to make progress. Maybe I'll even post some recordings eventually. I think I'm still in the honeymoon period where I'm dreaming big and not fully aware of the crushing reality, the *truth*, but for now, It's been a blast.

# Edit: Photos!

*behold my double chin!!!*

Dad's old Hondo Acoustic. Sorry dad, it's retiring.
![the old one](/images/20210121_194115.jpg)

Walmart guitar, aka Washburn Lyon
![the other old one](/images/20210121_194229.jpg)

Sweet black ESP Les Paul Special II
![mmm sweet les paul action](/images/20210121_194148.jpg)

Rogue Classic Spider Resonator (roundneck, also an acoustic)
![insert blues sounds here](/images/20210121_194015.jpg)

$230 Squier Precision Bass for $80
![bummmmmm babummmm](/images/20210121_193930.jpg)[^5]

[^1]: pun intended
[^2]: Rogue Classic Spider Resonator, Roundneck, Black, about the cheapest one I could find that wasn't just amazon garbage
[^3]: exactly on schedule, as Justin predicted
[^4]: possibly the feywild
[^5]: *c-c-combo breaker!*