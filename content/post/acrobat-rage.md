---
title: "Acrobat Rage"
date: 2019-03-19T07:44:45-05:00
tags: [game-design, blog, software, rant]
draft: false
featured_image: ""
hidehero: true
---

*This is about desktop publishing, not a Bard/Barbarian build.*

Oh my god I hate Acrobat.

I know that I say that I don't like ragging on things, and that I want to reduce worldsuck, but I really need to get this rant off my chest.

Wow.

So I guess every single publishing and print company on earth is basically beholden to Adobe. They have an utter monopoly on the tools you need to get content into print. I wonder how press and prepress/preflight companies feel about this. Probably okay, but still.

I don't want to pay Adobe any more money. I don't want to use their tools unless I absolutely must. I've worked very hard to [get Adobe software out of my publishing pipeline](/post/learning-to-love-document-generation/). I've replaced Photoshop with [Clip Studio Paint](http://www.clipstudio.net/en/). I've replaced Illustrator with [Affinity Designer](https://affinity.serif.com/en-us/designer/). I've replaced Indesign with [PrinceXML](https://www.princexml.com/) and [DocRaptor](http://docraptor.com/). I've changed the entire way I think about publishing and layout to get away from them.

**I do not want to rent the software that I depend on to do my job.**

Granted, sort of do that with DocRaptor, but I have the option to purchase a desktop license of PrinceXML if I choose.

But aside from all that, I've had numerous problems with Acrobat in the past, and they continue to this day. Every few times I use Acrobat, the program crashes without any warning or error message, then becomes utterly and profoundly unusable. Simply will not start. I've got a special folder in my tools for wiping, uninstalling, and reinstalling the goddamned thing, and I use it almost as much as I use Acrobat.

**This isn't the behavior I want from software that's mission critical for me.**

So when I finally hit the typical roadblock at the end of the publishing phase of #TheDawnline, I *of course* had to start using Acrobat again.

The short version is that despite all of my pipeline setup to produce PDFs without needing to touch Adobe, the printer that DTRPG uses *requires* an ancient, moldy, decrepit mummy of a PDF spec, which in turn forces me to flatten the PDF, which I can't do right in anything but Acrobat.

[removed: 17:32 of angry noises and gesticulations]

So yeah. I'm going to try other options for that process, because I have to, but I'm not excited about spending more money to find another patch.

[Alternativeto.net](https://alternativeto.net/) is like my third most visited site, but it takes time to figure out whether some new software will do what I want and not turn out to be more of a cancer than Acrobat. I've got shit to do, man.

It's really too bad that PrinceXML doesn't allow you to flatten the output, but it's also too bad that Lightning Source relies on such an archaic PDF standard.