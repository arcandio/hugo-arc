---
title: "Multiple Wip Friday"
date: 2020-06-19T10:17:44-05:00
tags: [blog, art, sketch, wip]
draft: false
featured_image: "/images/2020-06-19-10_05_48-Twitch-Chat-Overlay.png"
hidehero: true
---

![a fresh sketch](/images/2020-06-19-10_06_38-Twitch-Chat-Overlay.png)

I got a fresh commission Wednesday, and got started on it as soon as I possibly could, because the client actually used Fangtail as part of the reference art, and I have a penchant for big tails. Sketch seems to be going ok, I'm not super good at this body type yet, but practice is practice. I like how dynamic the pose is; the client had excellent reference art for each element.

![more lineart](/images/2020-06-19-10_05_48-Twitch-Chat-Overlay.png)

As I've talked about several times on [#lineart](/tags/lineart), I'm not really happy with my current abilites in that department, so this time I went out of my way to create a new lineart brush and make sure I had interesting line weight variations in the piece. Seems like it's going well so far, Meepo already looks better than most lineart I've ever done.