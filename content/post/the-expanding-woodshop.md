---
title: "The Expanding Woodshop"
date: 2020-09-11T12:34:08-05:00
tags: [blog, woodworking, home improvement]
draft: false
featured_image: "/images/20200831_215339.jpg"
hidehero: false
---

The last time I updated my blog, the woodshop was basically a toaster oven. Now, it's suddenly fall for a week, and I freaking love it. Time for blankets, snuggles, and woodworking.

> Aside: As much as I've been spinning my wheels the last few days without being able to get a project in the woodshop done, I really do appreciate being able to get out there and work on stuff with my hands. It's a very pleasant experience after spending all day on the computer. And while I do knock out plans in Blender fairly often, I've lately been finding that computer games don't hold my attention quite like they used to. I often spend my 8-10pm evenings out in the garage, tinkering and building, not to mention as much of my weekends as possible.

# It's almost getting out of hand.

Didn't I write previously that I'm worried about spending as much money as I have been on the woodshop? Well, I've spent more, and it's shaping up to be a habit.

I feel like pretty soon though the shop improvements will level off and I can focus more on building non-shop projects. And I know that's *exactly* what *everyone always* says. But I didn't say that it'd *stop*, just peter out.

Besides, I'm actually afraid that I might lose this hobby come wintertime, because our garage is completely un-insulated. It has drywall on the walls, but not the ceiling and in some places the only thing between me and the elements is the 40 year old wood siding.

Anyway, on with the show.

# More Wood Storage

First up, my wood storage unit, for short bits, cutoffs, and scrap.

![](/images/20200902_110838.jpg)

Here it is on its face, getting glued up with the longest clamps I have and two tie-down straps tied end to end. A couple sheets of spare floor tile complete the ensemble.

![](/images/20200831_215326.jpg)

What's silly about this thing is that the 1/2" plywood came UV coated so it's all glossy & shit. There wasn't anything between split, rough junk plywood sheathing and cabinetry-grade, so I opted for the one I thought I could actually assemble.

This thing was my first real attempt to build a box with rabbet joints, and that was probably a poor choice. Because it's not *quite* square in footprint, I had to cut a couple extra rabbets, leaving the inside somewhat awkward. Once the shelves were in, the whole thing became much more sturdy.

# French Cleats

French cleats are so cool. I put some up on tha back wall now that there's no wood leaning up against it.

For those who don't know, french cleats are simple boards cut at a 45&deg; angle, so that a mated piece slots in from above and pulls the rest of the item close to the wall. Since you're not fastening the container to the wall, you can rearrange any of your french cleat containers however you want as often as you want. And since you can pre-cut french cleat boards, you can have a scad of them waiting for you to finish constructing more containers for your tools or whatever. Very convenient.

![](/images/20200903_214801.jpg)
![](/images/20200903_210834.jpg)
![](/images/20200903_214755.jpg)
![](/images/20200902_211728.jpg)
![](/images/20200903_214806.jpg)
![](/images/20200904_170014.jpg)
![](/images/20200901_203337.jpg)

So I've spent a lot of time making french cleat tool storage containers, as you can see. I think so far I have cleats for

* wrenches
* hand saws & saw blades
* PPE
* apron, hoodies, & hats
* small clamps
* big clamps
* drill, driver, sanders/multitool[^1]
* circular saw
* manuals & plans
* panel goods hook handle
* levels, straight-edges, guide clamp

So that's a lot. That's a lot of stuff that now has a specific, **first order retrievable** home. I barely set any tools on my bench or table anymore, because they're better off on the wall, even if it takes me a couple steps to get to them. And things like the clamps I use all the time are within arm's reach of my temp workbench.

# Dust Collection

I chickened out on a boxfan air cleaner, but now I think I'm going to go ahead on that and get it made. I heard some horror stories about bad dust collection and lung damage. Dont want none a that.

In addition, I got some tubing and blast gates for my power tools and got them all hooked up last night. Check this out, I call it the hydra:

![](/images/20200910_212438.jpg)
![](/images/20200910_212427.jpg)
![](/images/20200910_212423.jpg)

Still need to work out a sweep, or attach an extra hose to the setup so I can get the stuff on the floor without having to change hoses.

I should probably also order more filters for the shopvac.

# Other Optimizations

* We drilled a hole in the wall and ran the power and air to the compressor in the shed. Now I can have the compressor running as much as I want and it doesn't bother anyone. Though I do forget to turn it off a lot now.
* I also hung a retracting air line from the cieling. Now I'm not about to trip and kill myself on 100ft of pneumatic hose.

# Actual Projects

Made a wooden mallet for chiseling & smacking. Haven't gotten around to engraving it, but the pattern's there at least. It's already in use.

![](/images/20200907_200912.jpg)

Made Liz a toolbox for the garden, because so far all the plastic ones have been the wrong size. It's not finished yet, so I'll post more photos when I get it done. Want to waterproof the thing since it's for the garden and will probably be mistreated.

Made with rabbet joints, captive slots, and through mortises with the chop saw, router table, mallet, chisel, mutlitool sander, & glue. Will be finished in cherry, and have brass feet for the corners if I can find them.

![](/images/20200907_210204.jpg)

Since we moved Liz's office down to the basement, I manufactured a better cat gate to maintain her peace and quiet.

Pretty simple deal, it's not fully framed to conserve weight, but the furring strips give the hinge screws something to bite in. This'll also be finished in cherry, once I've replaced the stock I knocked over and spilled all over the floor and workbench a few months back. Woops.

![](/images/20200910_212409.jpg)
![](/images/20200910_212403.jpg)

Welp, that about catches up to the present. Need to make *yet another menards run* for various components.

[^1]: This little $20 performax multitool is *fantastic*. It's a better sander than my hand sander, and it's pretty capable at cutting too. While it's kind of a pain to switch blades on, I'd rather not spend the extra $80 on a quick change mechanism, I'd rather put that on a different tool, or some more wood.