---
title: "Raffinas and Meepo Sketch"
date: 2020-06-17T10:17:09-05:00
tags: [art, sketch, wip, blog, weakness]
draft: false
featured_image: "/images/2020-06-17-10_03_14-Navigator.png"
hidehero: true
---

This piece is going to be in my "anime" style, which I haven't worked in in a long time. I keep forgetting that I'm not working on a painterly piece; when I'm working in *that* style, I don't have to worry about the linework of the sketch so much, but in these anime style ones, I really do, so I'm spending a lot of time on the lineart. Ironically, the sketch itself seems to be coming along pretty well, but I've still got [the sword of damocles of poor lineart over my head.](/post/magical-girl-lineart/)

![a sketch of a scene](/images/2020-06-17-10_03_14-Navigator.png)

Additionally, [I've just renewed my standard etsy commissions](https://www.etsy.com/listing/632930964/painterly-character-portrait-commission), so if you've been waiting, now's the time.

I've also been working really hard on TagRef2 lately, but I'll save talking about that for later, when I possibly have something people can test.