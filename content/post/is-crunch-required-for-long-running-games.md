---
title: "Is Crunch Required for Long-Running Games?"
date: 2019-02-26T11:07:39-05:00
tags: [blog, tabletop-rpgs, game-design]
draft: false
featured_image: ""
hidehero: true
---

I seem to write a lot of stuff phrased as a question when I intend to give a specific answer. This is not that. I'm not sure, but I've got some thoughts.

> Disclaimer: I'm sick and I have no plan for this post. It's going to be hard to follow.

I'd be tempted to say that "crunch" means "rules." The more detailed and specific the rules of the game are, the more "crunchy" the game is usually said to be. Specificity, detail, and math all seem to be important factors for a game to be considered crunchy. You can have a game with well-defined rules (and brooking no ambiguity in those rules) and *not* have a crunchy game, particularly if those rules are so broadly applicable that you don't spend much time "using" them.

I guess I'd rather define it as how much time in the actual play of a game is spent on *navigating the rules specifically.* You could contrast that with other activities in the game, such as narration, interaction, or whatever. Obviously that's not a complete list.

A lot of people in the [#ttrpg](/tags/tabletop-rpgs) community talk like games are *either* crunchy *or* fluffy, like there's no middle ground at all. To me, it seems like it's a sliding scale, with pure (non-game) roleplaying on one end, rules light games & storytelling games somewhere in the middle, and experience-point-collecting grinds on the other. You can often argue where a game fits on that continuum, often because there are different play styles and different interpretations of the rules.

The important thing, it seems to me, is that the crunchier the game, the importance the lore or "fluff" has on the play of the game. It's been pointed out that taking this to extremes is what a lot of powergamers do with their rpg characters: exploit the rules to the exclusion of sensible lore.

I don't mean to rag on powergamers *or* crunch, though. Obviously, rules are required to play a game, without rules, a game becomes a toy or hobby. And good, well-written rules are both of vital importance to an RPG and are hard to come by. It's basically impossible to point to a single rpg that has no mechanical flaws; even D&D has errata pages and rules clarifications. 

Anyway, that's what crunch *is.* Let's talk about whether it's necessary for long-form campaigns. I think I'm also questioning whether it's important for the longevity of an RPG on a meta sense; let me rephrase both as questions.

1. Is crunch required for long campaigns?
2. Is crunch required for long-term player interest in a game, *as a game?*

So number 2 is potentially *more* difficult to answer because it's not talking about running campaigns, but about whether players will keep coming back to the game after they've played or read it.

I'm not sure whether I can answer those two questions yet, but maybe I'm pursuing too black-and-white a question here.

Maybe it's just like everything else.

## Point of View: Absolutely Necessary

The naive part of me says yes, it's definitely integral to long-term success of a game, for several reasons.

* It's hard to have a long-running character that doesn't grow much, and if you don't have a lot of options, there's not much room to grow.
* It seems like the crunch-favoring crowd is the larger, more vocal sub-group in the ttrpg space.
* Without a lot of options for customization, it's not as much fun to play the game multiple times.
* It's hard to take a really light game seriously, regardless of whether it actually lasts a while.

Conversely...

* It seems like it's easy to stretch out a crunch-heavy game into oblivion because you're spending so much time on minutia.
* Not everyone likes spending all their time doing math to play the game. See GNS theory.
* Character customization seems like it can be balanced by customization of the scenario: you could play similar characters if you got to do so in a completely different situation.

## Point of View: Not necessary at all

Let's play devil's advocate for a minute.

* Most of the *meaningful* character growth is done by characterization rather than mechanics.
* If you could give players concrete rewards that mirror their story accomplishments, you'd be a long way towards motivating them to play just a campaign just as long as a crunch heavy game.
* Rarely are players motivated purely by avarice for some specific item, because most of the time they're not aware of what specific items exist in the game world before them to acquire. Thus, they're playing with the *expectation* of getting cool stuff.
  * Side note here: Maybe there's a mapping of RPG players similar to GNS theory that focuses on *player goals* rather than play styles: get cool stuff vs do cool things vs participate in cool stories? I don't know. Maybe another blog post.

Conversely...

* I don't know of any games that have successfully created well-defined, broadly-applicable reward system tied to story success rather than mechanical bonuses. It might exist, but I don't know of it. [Tweet](https://twitter.com/Voidspiral) or [toot](https://tabletop.social/@voidspiral) at me if you know one.
* The loot grind is strong in the ttrpg community, fueled not just by our own games, but also video games and other forms of instant gratification feedback.
* *Change* doesn't feel as exciting as *growth.* You can swap stats around or change which skills you have, but that'll never be as much of a reward as getting better at something. (Story of my life!)

## Analysis

I don't even know man. It seems like even story-oriented games will benefit from having crunchy, detailed rules, at least as long as they're intended to be played for longer periods. *Leveling up feels great.* If you spend 20 sessions in a game where you level up 20 times, you've had 200% the fun of a game where there are only 10 levels to get through in that same time.

I guess conversely, if you break up progression too much, it becomes less of a reward and more of a book-keeping chore. "Ah jeez, which skill will I put my one measly skill point into..."

And it'd take a *huge* and *very specific* survey to get an idea of what sort of campaigns you should design your own specific niche game around. So it's hard to nail down one variable of the N-Body Problem by saying "campaigns will be this length as our assumption, just so we can determine how often to give out rewards."

## My specific problems

A major reason I'm thinking about this is that I feel like Heroines of the First Age (A PbtA game) lacks some of that longevity because there aren't that many options to actually take. Sure, the DNs could go up arbitrarily high, and you could keep giving the players more and more xp, leading to more and more stats and moves, but it still seems like Oubliette Second Edition (a FATE Core-based game) had better progression to it. There as simply a lot of content there, and it was in discrete enough steps that you could feasibly get an O2E campaign to last quite a while. It helps that FATE has it's own very clear progression mechanics, and that I expanded that into a level-like system for that setting. I did a similar thing for HFA, but I'm not as confident in that: it seems fairly easy to take all the archetype moves you want, then be sort of lost for new moves to take.

Since I'm also working on other games, this also generalizes to them too. I'm not sure how much "room" there is in #Antum or #ATSD, and there sure as hell won't be a lot in #LostSoulBlues.

Maybe I should just write an Epic Level Handbook for HFA.

Conclusion: None.